/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Alexander
 */
@Entity
@Table(name = "asignarhab")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Asignarhab.findAll", query = "SELECT a FROM Asignarhab a"),
    @NamedQuery(name = "Asignarhab.findByAsignarhab", query = "SELECT a FROM Asignarhab a WHERE a.asignarhab = :asignarhab"),
    @NamedQuery(name = "Asignarhab.findByHoraE", query = "SELECT a FROM Asignarhab a WHERE a.horaE = :horaE"),
    @NamedQuery(name = "Asignarhab.findByHoraS", query = "SELECT a FROM Asignarhab a WHERE a.horaS = :horaS"),
    @NamedQuery(name = "Asignarhab.findByFecha", query = "SELECT a FROM Asignarhab a WHERE a.fecha = :fecha"),
    @NamedQuery(name = "Asignarhab.findByEstado", query = "SELECT a FROM Asignarhab a WHERE a.estado = :estado")})
public class Asignarhab implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Asignarhab")
    private Integer asignarhab;
    @Basic(optional = false)
    @Column(name = "HoraE")
    private String horaE;
    @Column(name = "HoraS")
    private String horaS;
    @Basic(optional = false)
    @Column(name = "Fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @Column(name = "estado")
    private String estado;
    @JoinColumn(name = "Habitacion", referencedColumnName = "idHabitacion")
    @ManyToOne(optional = false)
    private Habitacion habitacion;
    @JoinColumn(name = "Cliente", referencedColumnName = "Cliente")
    @ManyToOne(optional = false)
    private Cliente cliente;
    @JoinColumn(name = "usuario_usuario", referencedColumnName = "Usuario")
    @ManyToOne(optional = false)
    private Usuario usuarioUsuario;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "asignarHab")
    private List<Generarfactura> generarfacturaList;

    public Asignarhab() {
    }

    public Asignarhab(Integer asignarhab) {
        this.asignarhab = asignarhab;
    }

    public Asignarhab(Integer asignarhab, String horaE, Date fecha, String estado) {
        this.asignarhab = asignarhab;
        this.horaE = horaE;
        this.fecha = fecha;
        this.estado = estado;
    }

    public Integer getAsignarhab() {
        return asignarhab;
    }

    public void setAsignarhab(Integer asignarhab) {
        this.asignarhab = asignarhab;
    }

    public String getHoraE() {
        return horaE;
    }

    public void setHoraE(String horaE) {
        this.horaE = horaE;
    }

    public String getHoraS() {
        return horaS;
    }

    public void setHoraS(String horaS) {
        this.horaS = horaS;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Habitacion getHabitacion() {
        return habitacion;
    }

    public void setHabitacion(Habitacion habitacion) {
        this.habitacion = habitacion;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Usuario getUsuarioUsuario() {
        return usuarioUsuario;
    }

    public void setUsuarioUsuario(Usuario usuarioUsuario) {
        this.usuarioUsuario = usuarioUsuario;
    }

    @XmlTransient
    public List<Generarfactura> getGenerarfacturaList() {
        return generarfacturaList;
    }

    public void setGenerarfacturaList(List<Generarfactura> generarfacturaList) {
        this.generarfacturaList = generarfacturaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (asignarhab != null ? asignarhab.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Asignarhab)) {
            return false;
        }
        Asignarhab other = (Asignarhab) object;
        if ((this.asignarhab == null && other.asignarhab != null) || (this.asignarhab != null && !this.asignarhab.equals(other.asignarhab))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Dto.Asignarhab[ asignarhab=" + asignarhab + " ]";
    }
    
}
