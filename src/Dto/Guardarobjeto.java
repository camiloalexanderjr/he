/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Alexander
 */
@Entity
@Table(name = "guardarobjeto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Guardarobjeto.findAll", query = "SELECT g FROM Guardarobjeto g"),
    @NamedQuery(name = "Guardarobjeto.findByIdObjeto", query = "SELECT g FROM Guardarobjeto g WHERE g.idObjeto = :idObjeto"),
    @NamedQuery(name = "Guardarobjeto.findByNombre", query = "SELECT g FROM Guardarobjeto g WHERE g.nombre = :nombre"),
    @NamedQuery(name = "Guardarobjeto.findByDescripcion", query = "SELECT g FROM Guardarobjeto g WHERE g.descripcion = :descripcion")})
public class Guardarobjeto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "idObjeto")
    private String idObjeto;
    @Basic(optional = false)
    @Column(name = "Nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "Descripcion")
    private String descripcion;
    @OneToMany(mappedBy = "objeto")
    private List<Cliente> clienteList;

    public Guardarobjeto() {
    }

    public Guardarobjeto(String idObjeto) {
        this.idObjeto = idObjeto;
    }

    public Guardarobjeto(String idObjeto, String nombre, String descripcion) {
        this.idObjeto = idObjeto;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public String getIdObjeto() {
        return idObjeto;
    }

    public void setIdObjeto(String idObjeto) {
        this.idObjeto = idObjeto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<Cliente> getClienteList() {
        return clienteList;
    }

    public void setClienteList(List<Cliente> clienteList) {
        this.clienteList = clienteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idObjeto != null ? idObjeto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Guardarobjeto)) {
            return false;
        }
        Guardarobjeto other = (Guardarobjeto) object;
        if ((this.idObjeto == null && other.idObjeto != null) || (this.idObjeto != null && !this.idObjeto.equals(other.idObjeto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Dto.Guardarobjeto[ idObjeto=" + idObjeto + " ]";
    }
    
}
