/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Alexander
 */
@Entity
@Table(name = "habitacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Habitacion.findAll", query = "SELECT h FROM Habitacion h"),
    @NamedQuery(name = "Habitacion.findByIdHabitacion", query = "SELECT h FROM Habitacion h WHERE h.idHabitacion = :idHabitacion"),
    @NamedQuery(name = "Habitacion.findByCapacidad", query = "SELECT h FROM Habitacion h WHERE h.capacidad = :capacidad")})
public class Habitacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "idHabitacion")
    private String idHabitacion;
    @Basic(optional = false)
    @Column(name = "Capacidad")
    private int capacidad;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "habitacion")
    private List<Asignarhab> asignarhabList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "habitacionHabitacion")
    private List<Limpieza> limpiezaList;
    @JoinColumn(name = "Estado", referencedColumnName = "IdEstado")
    @ManyToOne(optional = false)
    private Estado estado;
    @JoinColumn(name = "TipoHab", referencedColumnName = "IdTipo")
    @ManyToOne(optional = false)
    private Tipohabitacion tipoHab;

    public Habitacion() {
    }

    public Habitacion(String idHabitacion) {
        this.idHabitacion = idHabitacion;
    }

    public Habitacion(String idHabitacion, int capacidad) {
        this.idHabitacion = idHabitacion;
        this.capacidad = capacidad;
    }

    public String getIdHabitacion() {
        return idHabitacion;
    }

    public void setIdHabitacion(String idHabitacion) {
        this.idHabitacion = idHabitacion;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    @XmlTransient
    public List<Asignarhab> getAsignarhabList() {
        return asignarhabList;
    }

    public void setAsignarhabList(List<Asignarhab> asignarhabList) {
        this.asignarhabList = asignarhabList;
    }

    @XmlTransient
    public List<Limpieza> getLimpiezaList() {
        return limpiezaList;
    }

    public void setLimpiezaList(List<Limpieza> limpiezaList) {
        this.limpiezaList = limpiezaList;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Tipohabitacion getTipoHab() {
        return tipoHab;
    }

    public void setTipoHab(Tipohabitacion tipoHab) {
        this.tipoHab = tipoHab;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idHabitacion != null ? idHabitacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Habitacion)) {
            return false;
        }
        Habitacion other = (Habitacion) object;
        if ((this.idHabitacion == null && other.idHabitacion != null) || (this.idHabitacion != null && !this.idHabitacion.equals(other.idHabitacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Dto.Habitacion[ idHabitacion=" + idHabitacion + " ]";
    }
    
}
