/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Alexander
 */
@Entity
@Table(name = "cafeteria")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cafeteria.findAll", query = "SELECT c FROM Cafeteria c"),
    @NamedQuery(name = "Cafeteria.findByIdCompra", query = "SELECT c FROM Cafeteria c WHERE c.idCompra = :idCompra"),
    @NamedQuery(name = "Cafeteria.findByCantidad", query = "SELECT c FROM Cafeteria c WHERE c.cantidad = :cantidad"),
    @NamedQuery(name = "Cafeteria.findByProducto", query = "SELECT c FROM Cafeteria c WHERE c.producto = :producto"),
    @NamedQuery(name = "Cafeteria.findByPrecio", query = "SELECT c FROM Cafeteria c WHERE c.precio = :precio"),
    @NamedQuery(name = "Cafeteria.findByFecha", query = "SELECT c FROM Cafeteria c WHERE c.fecha = :fecha")})
public class Cafeteria implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idCompra")
    private Integer idCompra;
    @Basic(optional = false)
    @Column(name = "Cantidad")
    private int cantidad;
    @Basic(optional = false)
    @Column(name = "Producto")
    private String producto;
    @Basic(optional = false)
    @Column(name = "Precio")
    private int precio;
    @Basic(optional = false)
    @Column(name = "Fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @JoinColumn(name = "usuario_usuario", referencedColumnName = "Usuario")
    @ManyToOne(optional = false)
    private Usuario usuarioUsuario;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cafeteriaCafeteria")
    private List<Caja> cajaList;

    public Cafeteria() {
    }

    public Cafeteria(Integer idCompra) {
        this.idCompra = idCompra;
    }

    public Cafeteria(Integer idCompra, int cantidad, String producto, int precio, Date fecha) {
        this.idCompra = idCompra;
        this.cantidad = cantidad;
        this.producto = producto;
        this.precio = precio;
        this.fecha = fecha;
    }

    public Integer getIdCompra() {
        return idCompra;
    }

    public void setIdCompra(Integer idCompra) {
        this.idCompra = idCompra;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Usuario getUsuarioUsuario() {
        return usuarioUsuario;
    }

    public void setUsuarioUsuario(Usuario usuarioUsuario) {
        this.usuarioUsuario = usuarioUsuario;
    }

    @XmlTransient
    public List<Caja> getCajaList() {
        return cajaList;
    }

    public void setCajaList(List<Caja> cajaList) {
        this.cajaList = cajaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCompra != null ? idCompra.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cafeteria)) {
            return false;
        }
        Cafeteria other = (Cafeteria) object;
        if ((this.idCompra == null && other.idCompra != null) || (this.idCompra != null && !this.idCompra.equals(other.idCompra))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Dto.Cafeteria[ idCompra=" + idCompra + " ]";
    }
    
}
