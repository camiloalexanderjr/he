/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Alexander
 */
@Entity
@Table(name = "caja")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Caja.findAll", query = "SELECT c FROM Caja c"),
    @NamedQuery(name = "Caja.findByIdEstado", query = "SELECT c FROM Caja c WHERE c.idEstado = :idEstado"),
    @NamedQuery(name = "Caja.findByFecha", query = "SELECT c FROM Caja c WHERE c.fecha = :fecha"),
    @NamedQuery(name = "Caja.findByUsarioUsuario", query = "SELECT c FROM Caja c WHERE c.usarioUsuario = :usarioUsuario"),
    @NamedQuery(name = "Caja.findByHoraInicio", query = "SELECT c FROM Caja c WHERE c.horaInicio = :horaInicio"),
    @NamedQuery(name = "Caja.findByHoraFin", query = "SELECT c FROM Caja c WHERE c.horaFin = :horaFin")})
public class Caja implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "idEstado")
    private String idEstado;
    @Basic(optional = false)
    @Column(name = "Fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @Column(name = "usario_usuario")
    private String usarioUsuario;
    @Basic(optional = false)
    @Column(name = "HoraInicio")
    private String horaInicio;
    @Basic(optional = false)
    @Column(name = "HoraFin")
    private String horaFin;
    @JoinColumn(name = "Cafeteria_Cafeteria", referencedColumnName = "idCompra")
    @ManyToOne(optional = false)
    private Cafeteria cafeteriaCafeteria;
    @JoinColumn(name = "Factura_Factura", referencedColumnName = "idFactura")
    @ManyToOne(optional = false)
    private Generarfactura facturaFactura;

    public Caja() {
    }

    public Caja(String idEstado) {
        this.idEstado = idEstado;
    }

    public Caja(String idEstado, Date fecha, String usarioUsuario, String horaInicio, String horaFin) {
        this.idEstado = idEstado;
        this.fecha = fecha;
        this.usarioUsuario = usarioUsuario;
        this.horaInicio = horaInicio;
        this.horaFin = horaFin;
    }

    public String getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(String idEstado) {
        this.idEstado = idEstado;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getUsarioUsuario() {
        return usarioUsuario;
    }

    public void setUsarioUsuario(String usarioUsuario) {
        this.usarioUsuario = usarioUsuario;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }

    public Cafeteria getCafeteriaCafeteria() {
        return cafeteriaCafeteria;
    }

    public void setCafeteriaCafeteria(Cafeteria cafeteriaCafeteria) {
        this.cafeteriaCafeteria = cafeteriaCafeteria;
    }

    public Generarfactura getFacturaFactura() {
        return facturaFactura;
    }

    public void setFacturaFactura(Generarfactura facturaFactura) {
        this.facturaFactura = facturaFactura;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEstado != null ? idEstado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Caja)) {
            return false;
        }
        Caja other = (Caja) object;
        if ((this.idEstado == null && other.idEstado != null) || (this.idEstado != null && !this.idEstado.equals(other.idEstado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Dto.Caja[ idEstado=" + idEstado + " ]";
    }
    
}
