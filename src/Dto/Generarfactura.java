/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Alexander
 */
@Entity
@Table(name = "generarfactura")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Generarfactura.findAll", query = "SELECT g FROM Generarfactura g"),
    @NamedQuery(name = "Generarfactura.findByIdFactura", query = "SELECT g FROM Generarfactura g WHERE g.idFactura = :idFactura"),
    @NamedQuery(name = "Generarfactura.findByFecha", query = "SELECT g FROM Generarfactura g WHERE g.fecha = :fecha"),
    @NamedQuery(name = "Generarfactura.findByNombreFactura", query = "SELECT g FROM Generarfactura g WHERE g.nombreFactura = :nombreFactura")})
public class Generarfactura implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idFactura")
    private Integer idFactura;
    @Basic(optional = false)
    @Column(name = "Fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @Column(name = "NombreFactura")
    private String nombreFactura;
    @JoinColumn(name = "usuario_usuario", referencedColumnName = "Usuario")
    @ManyToOne(optional = false)
    private Usuario usuarioUsuario;
    @JoinColumn(name = "AsignarHab", referencedColumnName = "Asignarhab")
    @ManyToOne(optional = false)
    private Asignarhab asignarHab;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "facturaFactura")
    private List<Caja> cajaList;

    public Generarfactura() {
    }

    public Generarfactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Generarfactura(Integer idFactura, Date fecha, String nombreFactura) {
        this.idFactura = idFactura;
        this.fecha = fecha;
        this.nombreFactura = nombreFactura;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getNombreFactura() {
        return nombreFactura;
    }

    public void setNombreFactura(String nombreFactura) {
        this.nombreFactura = nombreFactura;
    }

    public Usuario getUsuarioUsuario() {
        return usuarioUsuario;
    }

    public void setUsuarioUsuario(Usuario usuarioUsuario) {
        this.usuarioUsuario = usuarioUsuario;
    }

    public Asignarhab getAsignarHab() {
        return asignarHab;
    }

    public void setAsignarHab(Asignarhab asignarHab) {
        this.asignarHab = asignarHab;
    }

    @XmlTransient
    public List<Caja> getCajaList() {
        return cajaList;
    }

    public void setCajaList(List<Caja> cajaList) {
        this.cajaList = cajaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFactura != null ? idFactura.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Generarfactura)) {
            return false;
        }
        Generarfactura other = (Generarfactura) object;
        if ((this.idFactura == null && other.idFactura != null) || (this.idFactura != null && !this.idFactura.equals(other.idFactura))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Dto.Generarfactura[ idFactura=" + idFactura + " ]";
    }
    
}
