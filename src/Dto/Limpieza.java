/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Alexander
 */
@Entity
@Table(name = "limpieza")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Limpieza.findAll", query = "SELECT l FROM Limpieza l"),
    @NamedQuery(name = "Limpieza.findByIdLimpieza", query = "SELECT l FROM Limpieza l WHERE l.idLimpieza = :idLimpieza"),
    @NamedQuery(name = "Limpieza.findByHoraInicio", query = "SELECT l FROM Limpieza l WHERE l.horaInicio = :horaInicio"),
    @NamedQuery(name = "Limpieza.findByHoraFin", query = "SELECT l FROM Limpieza l WHERE l.horaFin = :horaFin"),
    @NamedQuery(name = "Limpieza.findByObservacion", query = "SELECT l FROM Limpieza l WHERE l.observacion = :observacion"),
    @NamedQuery(name = "Limpieza.findByFecha", query = "SELECT l FROM Limpieza l WHERE l.fecha = :fecha"),
    @NamedQuery(name = "Limpieza.findByEstado", query = "SELECT l FROM Limpieza l WHERE l.estado = :estado")})
public class Limpieza implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idLimpieza")
    private Integer idLimpieza;
    @Basic(optional = false)
    @Column(name = "HoraInicio")
    private String horaInicio;
    @Column(name = "HoraFin")
    private String horaFin;
    @Basic(optional = false)
    @Column(name = "Observacion")
    private String observacion;
    @Basic(optional = false)
    @Column(name = "Fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Basic(optional = false)
    @Column(name = "estado")
    private String estado;
    @JoinColumn(name = "Habitacion_Habitacion", referencedColumnName = "idHabitacion")
    @ManyToOne(optional = false)
    private Habitacion habitacionHabitacion;
    @JoinColumn(name = "NombreEmpleado", referencedColumnName = "Nombre")
    @ManyToOne(optional = false)
    private Empleado nombreEmpleado;

    public Limpieza() {
    }

    public Limpieza(Integer idLimpieza) {
        this.idLimpieza = idLimpieza;
    }

    public Limpieza(Integer idLimpieza, String horaInicio, String observacion, Date fecha, String estado) {
        this.idLimpieza = idLimpieza;
        this.horaInicio = horaInicio;
        this.observacion = observacion;
        this.fecha = fecha;
        this.estado = estado;
    }

    public Integer getIdLimpieza() {
        return idLimpieza;
    }

    public void setIdLimpieza(Integer idLimpieza) {
        this.idLimpieza = idLimpieza;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Habitacion getHabitacionHabitacion() {
        return habitacionHabitacion;
    }

    public void setHabitacionHabitacion(Habitacion habitacionHabitacion) {
        this.habitacionHabitacion = habitacionHabitacion;
    }

    public Empleado getNombreEmpleado() {
        return nombreEmpleado;
    }

    public void setNombreEmpleado(Empleado nombreEmpleado) {
        this.nombreEmpleado = nombreEmpleado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLimpieza != null ? idLimpieza.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Limpieza)) {
            return false;
        }
        Limpieza other = (Limpieza) object;
        if ((this.idLimpieza == null && other.idLimpieza != null) || (this.idLimpieza != null && !this.idLimpieza.equals(other.idLimpieza))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Dto.Limpieza[ idLimpieza=" + idLimpieza + " ]";
    }
    
}
