/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import Conexion.Conexion;
import Dao.HabitacionJpaController;
import Dto.Habitacion;
import Vista.BotonesPrincipal.PHabitacion;
import java.awt.Font;
import java.util.List;
import javax.swing.JLabel;

/**
 *
 * @author Alexander
 */
public class HabitacionCon {

    Conexion con = Conexion.getConexion();

    HabitacionJpaController x = new HabitacionJpaController(con.getBd());
    List<Habitacion> clientes = x.findHabitacionEntities();

    Boton[][] botones = new Boton[4][5];
    int filas = 4;
    int columnas = 5;

    public HabitacionCon() {
        Botones();
        letras();
//           
    }

    int cont = 201;

    int cont3 = 1;

    private void Botones() {

        for (int columna = 0; columna < columnas; columna++) {
            for (int fila = 0; fila < filas; fila++) {

                botones[fila][columna] = new Boton((fila * 100) + 100, (columna * 100) + 50, 90, 100);
                botones[fila][columna].setVisible(true);;

                PHabitacion.contenedor.add(botones[fila][columna]);

                PHabitacion.contenedor.repaint();
                if (cont == 20) {
                    botones[fila][columna].setVisible(false);
                }

                for (Habitacion h : clientes) {
                    if (cont == 209) {
                    cont += 102;
                }
                    if (h.getIdHabitacion().equalsIgnoreCase(cont + "") && h.getEstado().getIdEstado().equalsIgnoreCase("ocupado")) {

                        botones[fila][columna].setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMG/ocupado.png"))); // NOI18N

                    } else if (h.getIdHabitacion().equalsIgnoreCase(cont + "") && h.getEstado().getIdEstado().equalsIgnoreCase("disponible")) {

                        botones[fila][columna].setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMG/disponible.png"))); // NOI18N

                    } else if (h.getIdHabitacion().equalsIgnoreCase(cont + "") && h.getEstado().getIdEstado().equalsIgnoreCase("limpieza")) {

                        botones[fila][columna].setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMG/limpieza.png"))); // NOI18N
                   
                    } else if (h.getIdHabitacion().equalsIgnoreCase(cont + "") && h.getEstado().getIdEstado().equalsIgnoreCase("inactivo")) {

                        botones[fila][columna].setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMG/inactivo.png"))); // NOI18N
                   
                    }
                }

                cont++;

            }
        }
    }

    int cont2 = 201;
    int a = 1;

    private void letras() {
        for (int columna = 0; columna < columnas; columna++) {
            for (int fila = 0; fila < filas; fila++) {

                JLabel L = new JLabel("" + cont2);
                L.setBounds((fila * 100) + 115, (columna * 100) + 60, 70, 70);
                L.setVisible(true);

                Font auxFont = L.getFont();
                L.setFont(new Font(auxFont.getFontName(), auxFont.getStyle(), 30));
                System.out.println();
                PHabitacion.numero.add(L);
                PHabitacion.numero.repaint();
                cont2++;

                if (cont2 == 209) {
                    cont2 += 102;
                }
                if (a == 20) {
                    L.setVisible(false);
                }
                a++;
            }
        }

    }
}
