/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import Conexion.Conexion;
import Dao.ClienteJpaController;
import Dao.GuardarobjetoJpaController;
import Dto.Cliente;
import Dto.Guardarobjeto;
import Vista.BotonesPrincipal.PObjeto;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Alexander
 */
public class ObjetoCon {

    PObjeto p;

    Conexion con = Conexion.getConexion();

    ClienteJpaController x = new ClienteJpaController(con.getBd());
    List<Cliente> clientes = x.findClienteEntities();

    GuardarobjetoJpaController n = new GuardarobjetoJpaController(con.getBd());
    List<Guardarobjeto> objetos = n.findGuardarobjetoEntities();

    public ObjetoCon() {
    }
            boolean t;
    public void Buscar(String cedula) {

        for (Guardarobjeto o : objetos) {
            if (o.getIdObjeto().equals(cedula)) {
                JOptionPane.showMessageDialog(null, "Objeto encontrado");
                p.cedula.setText(o.getIdObjeto());
                p.objeto.setText(o.getNombre());
                p.observa.setText(o.getDescripcion());
               int a= JOptionPane.showConfirmDialog(null,"Desea sacar un Objeto?"," ", JOptionPane.YES_NO_OPTION);
                    if(a==0){
                        String objeto=JOptionPane.showInputDialog("Digite los objetos a guardar");
                        String obs=JOptionPane.showInputDialog("Digite las nuevas observaciones");
                        o.setNombre(objeto);
                        o.setDescripcion(obs);
                        try {
                            n.edit(o);
                            p.objeto.setText(o.getNombre());
                            JOptionPane.showMessageDialog(null,"Información Actualizada");
                        } catch (Exception e) {
                            System.out.println("Error   "+ e);
                        }
                    }
                }
            
                }
            }

        
    

    public void GuardarObj(String cedula, String nombre, String observa) {

        for (Cliente c : clientes) {
            if (c.getCliente().equalsIgnoreCase(cedula) && c.getObjeto()==null) {
                Guardarobjeto obj = new Guardarobjeto();
                
                obj.setIdObjeto(cedula);
                obj.setDescripcion(observa);
                obj.setNombre(nombre);

                try {
                    n.create(obj);
                    c.setObjeto(obj);
                    x.edit(c);
                    JOptionPane.showMessageDialog(null, "Objeto Guardado");

                } catch (Exception e) {
                  JOptionPane.showMessageDialog(null, "No se pudo guardar");

                }
            }
            else if(c.getCliente().equalsIgnoreCase(cedula) && c.getObjeto()!=null) {   
                JOptionPane.showMessageDialog(null, "El cliente "+ c.getCliente()+" ya tiene asignado un objeto");

                
                
            }
        }
    }

}
