/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import Conexion.Conexion;
import Dao.AsignarhabJpaController;
import Dao.ClienteJpaController;
import Dto.Asignarhab;
import Dto.Cliente;
import Vista.BotonesPrincipal.PClientes;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Alexander
 */
public class ClienteCon {

    Conexion con = Conexion.getConexion();

    ClienteJpaController x = new ClienteJpaController(con.getBd());
    List<Cliente> clientes = x.findClienteEntities();

    AsignarhabJpaController h = new AsignarhabJpaController(con.getBd());
    List<Asignarhab> hab = h.findAsignarhabEntities();

    public ClienteCon() {

    }

    public void RegistrarCliente(String cedula, String nombre, String apellido, String telefono) {
        Cliente c = new Cliente();

        c.setCliente(cedula);
        c.setNombre(nombre);
        c.setApellido(apellido);
        c.setTelefono(telefono);
        c.setFecha(new Date());
        try {
            x.create(c);
            JOptionPane.showMessageDialog(null, "Cliente Registrado");

            // TODO add your handling code here:
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "El usuario " + c.getCliente() + "  ya se encuentra registrado");
            Logger.getLogger(PClientes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void BuscarClientes(String cedula) {

        for (Cliente c : clientes) {
            if (c.getCliente().equalsIgnoreCase(cedula)) {
                JOptionPane.showMessageDialog(null, "Cliente Registrado");

            }

        }
    }
}
