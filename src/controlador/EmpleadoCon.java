/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import Conexion.Conexion;
import Dao.EmpleadoJpaController;
import Dto.Empleado;
import Vista.BotonesPrincipal.PClientes;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Alexander
 */
public class EmpleadoCon {

    Conexion con = Conexion.getConexion();

    EmpleadoJpaController x = new EmpleadoJpaController(con.getBd());
    List<Empleado> emp = x.findEmpleadoEntities();

    public EmpleadoCon() {

    }

    public void RegistrarEmpleado(String cedula, String nombre, String apellido, String telefono, String direccion) {
        Empleado c = new Empleado();

        c.setCedula(cedula);
        c.setNombre(nombre);
        c.setApellido(apellido);
        c.setTelefono(telefono);
        c.setDireccion(direccion);
        c.setFecha(new Date());
        try {
            x.create(c);
            JOptionPane.showMessageDialog(null, "Empleado Registrado");

            // TODO add your handling code here:
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "El usuario " + c.getNombre() + "  ya se encuentra registrado");
            Logger.getLogger(PClientes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void BuscarEmpleado(String cedula) {

        for (Empleado c : emp) {
            if (c.getCedula().equalsIgnoreCase(cedula)) {
                JOptionPane.showMessageDialog(null, "Empleado Encontrado");

            }
        }
    }

}
