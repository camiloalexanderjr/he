/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import Conexion.Conexion;
import Dao.EmpleadoJpaController;
import Dao.EstadoJpaController;
import Dao.HabitacionJpaController;
import Dao.LimpiezaJpaController;
import Dto.Empleado;
import Dto.Estado;
import Dto.Habitacion;
import Dto.Limpieza;

import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Alexander
 */
public class LimpiezaCon {

    Conexion con = Conexion.getConexion();

    LimpiezaJpaController n = new LimpiezaJpaController(con.getBd());
    EmpleadoJpaController m = new EmpleadoJpaController(con.getBd());

    EstadoJpaController es = new EstadoJpaController(con.getBd());
    HabitacionJpaController x = new HabitacionJpaController(con.getBd());

    List<Habitacion> habs = x.findHabitacionEntities();
    List<Empleado> emp = m.findEmpleadoEntities();
    List<Estado> estados = es.findEstadoEntities();
    List<Limpieza> limp = n.findLimpiezaEntities();

    public LimpiezaCon() {
    }

    Estado e = new Estado();

    public void RegistrarLimp(String nombre, String hab, String hI) {
        Limpieza l = new Limpieza();

        for (Empleado c : emp) {
            if (c.getNombre().equalsIgnoreCase(nombre)) {

                l.setNombreEmpleado(c);
                l.setFecha(new Date());
                l.setEstado("activo");
            }
        }
        l.setHoraInicio(hI);

        l.setObservacion("");

        for (Habitacion c : habs) {
            if (c.getIdHabitacion().equalsIgnoreCase(hab) && (c.getEstado().getIdEstado().equalsIgnoreCase("disponible") || c.getEstado().getIdEstado().equalsIgnoreCase("inactivo"))) {
                l.setHabitacionHabitacion(c);
                for (Estado e : estados) {
                    if (e.getIdEstado().equalsIgnoreCase("limpieza")) {

                        c.setEstado(e);

                    }
                }

                try {
                    x.edit(c);
                    n.create(l);
                    JOptionPane.showMessageDialog(null, "Asginación Creada");

                } catch (Exception e) {

                    System.out.println("Error  " + e);
                }

            }
        }
    }
//

    public void horaFinal(String hab) {
        for (Habitacion c : habs) {

            for (Limpieza l : limp) {
                if (l.getHabitacionHabitacion().getIdHabitacion().equals(hab) && c.getIdHabitacion().equals(hab) && l.getEstado().equalsIgnoreCase("activo")) {

                    for (Estado e : estados) {
                        if (e.getIdEstado().equalsIgnoreCase("disponible")) {
                            c.setEstado(e);
                        }
                    }
                    l.setEstado("desactivado");
                    String obs = JOptionPane.showInputDialog("Observaciones");
                    l.setObservacion(obs);
                    String hf = JOptionPane.showInputDialog("Hora final");
                    l.setHoraFin(hf);

                    try {
                        x.edit(c);
                        n.edit(l);
                        JOptionPane.showMessageDialog(null, "Limpieza Finalizada");
                        JOptionPane.showMessageDialog(null, "Habitacion disponible");

                    } catch (Exception e) {
                        System.out.println("Error  " + e);
                    }
                }

            }

        }
    }

}
