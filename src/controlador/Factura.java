/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import Conexion.Conexion;
import Dao.AsignarhabJpaController;
import Dao.EstadoJpaController;
import Dao.GenerarfacturaJpaController;
import Dao.HabitacionJpaController;
import Dto.Asignarhab;
import Dto.Estado;
import Dto.Generarfactura;
import Dto.Habitacion;
import Vista.BotonesPrincipal.PFactura;
import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author Alexander
 */
public class Factura {

    Conexion con = Conexion.getConexion();
    HabitacionJpaController hl = new HabitacionJpaController(con.getBd());
    AsignarhabJpaController x = new AsignarhabJpaController(con.getBd());
    List<Asignarhab> asign = x.findAsignarhabEntities();
    List<Habitacion> habitacion = hl.findHabitacionEntities();

    ArrayList<String> lista = new ArrayList<>();

    Generarfactura g = new Generarfactura();
    GenerarfacturaJpaController gf = new GenerarfacturaJpaController(con.getBd());
    List<Generarfactura> listaFactura = gf.findGenerarfacturaEntities();

    public Factura() {

    }

    int cont = 0;

    public void mostrar(String numero) {

        for (Asignarhab h : asign) {
            if (h.getHabitacion().getIdHabitacion().equalsIgnoreCase(numero)) {
                cont++;

            }
        }
    }

    public String[][] buscar(String numero) {

        int i = 0;
        int c = 0;

        String matriz[][] = new String[cont][6];

        for (Asignarhab h : asign) {

            if (h.getHabitacion().getIdHabitacion().equalsIgnoreCase(numero) && h.getEstado().equalsIgnoreCase("activo")) {

                lista.add(h.getAsignarhab() + "");
                matriz[i][0] = lista.get(c);
                c++;
                lista.add(h.getHabitacion().getIdHabitacion());
                matriz[i][1] = lista.get(c);
                c++;
                lista.add(h.getCliente().getCliente());
                matriz[i][2] = lista.get(c);
                c++;
                lista.add(h.getCliente().getNombre());
                matriz[i][3] = lista.get(c);
                c++;
                lista.add(h.getCliente().getApellido());
                matriz[i][4] = lista.get(c);
                c++;
                lista.add(h.getCliente().getTelefono() + "");
                matriz[i][5] = lista.get(c);
                c++;

                i++;

            }

        }
        cont = 0;
        i = 0;
        c = 0;

        return matriz;
    }

    String ruta;

    static int idFactura;

    EstadoJpaController es = new EstadoJpaController(con.getBd());
    List<Estado> estados = es.findEstadoEntities();

    public String CrearFactura(int numero) {

        String msj = "";

        for (Asignarhab as : asign) {
            if (as.getAsignarhab().equals(numero)) {

                g.setAsignarHab(as);
                g.setFecha(new Date());
                g.setUsuarioUsuario(new Loguin().user);
                g.setNombreFactura("");

                editar(as.getHabitacion().getIdHabitacion());
                try {
                    x.edit(as);
                    gf.create(g);
                    idFactura = g.getIdFactura();
                    msj = "Creado";
                    System.out.println(as.getEstado() + "-------------");
                    System.out.println(as.getHabitacion().getEstado().getIdEstado() + "-------------");

                } catch (Exception ex) {

                    JOptionPane.showMessageDialog(null, "No creado");
                    msj = "No creado";

                }
            }

        }
        return msj;

    }

    private void editar(String n) {
        for (Asignarhab a : asign) {
            if (a.getHabitacion().getIdHabitacion().equalsIgnoreCase(n)) {
                a.setEstado("desactivado");
                try {
                    x.edit(a);
                    System.out.println("editado");
                } catch (Exception e) {
                    System.out.println("error " + e);
                }
            }
        }

        for (Habitacion h : habitacion) {
            if (h.getIdHabitacion().equalsIgnoreCase(n)) {
                for (Estado e : estados) {
                    if (e.getIdEstado().equalsIgnoreCase("inactivo")) {
                        h.setEstado(e);
                        try {

                            hl.edit(h);
                        } catch (Exception ex) {
                            System.out.println("error" + ex);
                        }
                    }
                }
            }
        }
    }

    String hotel = "      Hotel Esteban";
    String nit = "    NIT  37391919-4";
    String dire = "  calle 1 #6-74  Callejón";

    public String llenarFactura() {

        String datos = "";
        for (Generarfactura g : listaFactura) {
            if (g.getIdFactura().equals(idFactura)) {
                datos = "   Cliente: "+g.getAsignarHab().getCliente().getNombre() + " " + g.getAsignarHab().getCliente().getApellido() + "\n" + "     Numero Habitacion:  " + g.getAsignarHab().getHabitacion().getIdHabitacion() + "\n" + "       precio:   " + g.getAsignarHab().getHabitacion().getTipoHab().getPrecio() + "\n" + "\n" + "\n" + "\n" + hotel + "\n" + dire + "\n" + nit;

            }

        }
        return datos;
    }

    public String Imprimir() {
        String msj = "";
        JFileChooser dlg = new JFileChooser();
        int option = dlg.showSaveDialog(new PFactura());

        if (option == JFileChooser.APPROVE_OPTION) {

            File f = dlg.getSelectedFile();
            ruta = f.toString();
            editarNombre(f.getName());
        }
        try {

            FileOutputStream archivo = new FileOutputStream(ruta + ".pdf");
            Document doc = new Document();
            PdfWriter.getInstance(doc, archivo);
            doc.open();
            doc.add(new Paragraph(llenarFactura()));
            doc.close();
            JOptionPane.showMessageDialog(null, "Factura Creada");
            msj = "Creado";

        } catch (Exception e) {

            System.out.println("error ----   " + e);
            msj = "no creado";

        }
        return msj;
    }

    private void editarNombre(String nombre) {

        for (Generarfactura g : listaFactura) {
            if (g.getIdFactura().equals(idFactura)) {
                g.setNombreFactura(nombre);
                try {
                    gf.edit(g);
                } catch (Exception e) {
                }

            }

        }
    }

}
