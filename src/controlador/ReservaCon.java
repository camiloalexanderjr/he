/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import Conexion.Conexion;
import Dao.ClienteJpaController;
import Dao.ReservaJpaController;
import Dto.Cliente;
import Dto.Reserva;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Alexander
 */
public class ReservaCon {

    ArrayList<Reserva> lista = new ArrayList<>();

    Conexion con = Conexion.getConexion();

    ReservaJpaController x = new ReservaJpaController(con.getBd());
    ClienteJpaController c = new ClienteJpaController(con.getBd());

    List<Cliente> cliente = c.findClienteEntities();
    List<Reserva> res = x.findReservaEntities();

    public ReservaCon() {
        
        contador();
        
    }

    public void reserva(String cedula, String hab, String fecha) {
        for (Cliente cl : cliente) {

            if (cl.getCliente().equalsIgnoreCase(cedula)) {
                Reserva n = new Reserva();
                n.setCliente(cl.getNombre() + " " + cl.getApellido());
                n.setHabitacion(hab);
                n.setFecha(fecha);
                n.setUsuariousuario(new Loguin().user);
                try {
                    x.create(n);
                    JOptionPane.showMessageDialog(null, "Reservación Creada");
                } catch (Exception e) {
                    System.out.println("Error  " + e);
                }
            }
        }
    }

    int cont = 0;

    private void contador() {
        for (Reserva r : res) {
            cont++;
            lista.add(r);
        }
    }

    public String[][] mostrar() {

        String matriz[][] = new String[cont][3];

        for (int j = 0; j < lista.size(); j++) {

            matriz[j][0] = lista.get(j).getCliente() + "";
            matriz[j][1] = lista.get(j).getHabitacion();
            matriz[j][2] = lista.get(j).getFecha();
        }
        return matriz;
    }

}
