/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import Conexion.Conexion;
import Dao.UsuarioJpaController;
import Dto.Usuario;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Alexander
 */
public class UsuarioCRUD {

    Conexion con = Conexion.getConexion();
    UsuarioJpaController x = new UsuarioJpaController(con.getBd());

    List<Usuario> U = x.findUsuarioEntities();

    public UsuarioCRUD() {
        mostrar();
        buscar();

    }

    public void ActualizarPass(String user) {
        for (Usuario c : U) {
            if (c.getUsuario().equalsIgnoreCase(user)) {

                c.setContrasena(JOptionPane.showInputDialog("Digite la nueva Contraseña"));
                try {
                    x.edit(c);
                    JOptionPane.showMessageDialog(null, "Contraseña Actualizada");
                } catch (Exception e) {
                }
            }
        }
    }
    
    public void Desactivar(String user) {
        for (Usuario c : U) {
            if (c.getUsuario().equalsIgnoreCase(user)) {
                c.setEstado("Desactivado");
                
                try {
                    x.edit(c);
                    JOptionPane.showMessageDialog(null, "El usuario ha sido Desactivado");
                } catch (Exception e) {
                }
            }
        }
    }
    
    public void Activar(String user) {
        for (Usuario c : U) {
            if (c.getUsuario().equalsIgnoreCase(user)) {
                c.setEstado("Activo");
                
                try {
                    x.edit(c);
                    JOptionPane.showMessageDialog(null, "El usuario ha sido Activado");
                } catch (Exception e) {
                }
            }
        }
    }

    public void CrearUsuario(String user, String pass) {
        Usuario u = new Usuario();

        u.setUsuario(user);
        u.setContrasena(pass);
        u.setEstado("Activo");
        try {

            x.create(u);
            JOptionPane.showMessageDialog(null, "Usuario Creado");

        } catch (Exception e) {
        }
    }

    ArrayList<String> lista = new ArrayList<String>();

    int cont = 0;

    public void mostrar() {

        for (Usuario usuario : U) {
            cont++;

        }
    }

    public String[][] buscar() {

        int i = 0;
        int c = 0;
        String matriz[][] = new String[cont][3];

        for (Usuario usuario : U) {
            lista.add(usuario.getUsuario());
            matriz[i][0] = lista.get(c);
            c++;

            lista.add(usuario.getContrasena());
            matriz[i][1] = lista.get(c);
            c++;

            lista.add(usuario.getEstado());
            matriz[i][2]=lista.get(c);

            c++;

            i++;

        }
        i = 0;
        c = 0;

        return matriz;
    }

}
