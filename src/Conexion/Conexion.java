/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conexion;

/**
 *
 * @author Daniel
 */
import java.sql.Connection;
import java.sql.DriverManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author estudiante
 */
public class Conexion {

    private static Conexion conexion;
    private final EntityManagerFactory bd;

    public Conexion() {
        this.bd = Persistence.createEntityManagerFactory("HEPU");

    }

    public static Conexion getConexion() {
        if (conexion == null) {
            conexion = new Conexion();
        }
        return conexion;
    }

    public EntityManagerFactory getBd() {
        return bd;
    }
       static Connection cn;
    
    public  Connection getConexion2() {
        try {
            //?useUnicode=true&amp;useJDBCCompliantTimezoneShift=true&amp;useLegacyDatetimeCode=false&amp;serverTimezone=UTC
            String url = "jdbc:mysql://localhost:3306/hotel";
            String user = "root";
            String pass = "";
            Class.forName("com.mysql.jdbc.Driver");

            cn = DriverManager.getConnection(url, user, pass);

        } catch (Exception e) {

            System.err.println("error en conexion" + e);
        }
        return cn;
    }

}
