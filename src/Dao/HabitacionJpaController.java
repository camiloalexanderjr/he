/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Dao.exceptions.IllegalOrphanException;
import Dao.exceptions.NonexistentEntityException;
import Dao.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Dto.Estado;
import Dto.Tipohabitacion;
import Dto.Asignarhab;
import Dto.Habitacion;
import java.util.ArrayList;
import java.util.List;
import Dto.Limpieza;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Alexander
 */
public class HabitacionJpaController implements Serializable {

    public HabitacionJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Habitacion habitacion) throws PreexistingEntityException, Exception {
        if (habitacion.getAsignarhabList() == null) {
            habitacion.setAsignarhabList(new ArrayList<Asignarhab>());
        }
        if (habitacion.getLimpiezaList() == null) {
            habitacion.setLimpiezaList(new ArrayList<Limpieza>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Estado estado = habitacion.getEstado();
            if (estado != null) {
                estado = em.getReference(estado.getClass(), estado.getIdEstado());
                habitacion.setEstado(estado);
            }
            Tipohabitacion tipoHab = habitacion.getTipoHab();
            if (tipoHab != null) {
                tipoHab = em.getReference(tipoHab.getClass(), tipoHab.getIdTipo());
                habitacion.setTipoHab(tipoHab);
            }
            List<Asignarhab> attachedAsignarhabList = new ArrayList<Asignarhab>();
            for (Asignarhab asignarhabListAsignarhabToAttach : habitacion.getAsignarhabList()) {
                asignarhabListAsignarhabToAttach = em.getReference(asignarhabListAsignarhabToAttach.getClass(), asignarhabListAsignarhabToAttach.getAsignarhab());
                attachedAsignarhabList.add(asignarhabListAsignarhabToAttach);
            }
            habitacion.setAsignarhabList(attachedAsignarhabList);
            List<Limpieza> attachedLimpiezaList = new ArrayList<Limpieza>();
            for (Limpieza limpiezaListLimpiezaToAttach : habitacion.getLimpiezaList()) {
                limpiezaListLimpiezaToAttach = em.getReference(limpiezaListLimpiezaToAttach.getClass(), limpiezaListLimpiezaToAttach.getIdLimpieza());
                attachedLimpiezaList.add(limpiezaListLimpiezaToAttach);
            }
            habitacion.setLimpiezaList(attachedLimpiezaList);
            em.persist(habitacion);
            if (estado != null) {
                estado.getHabitacionList().add(habitacion);
                estado = em.merge(estado);
            }
            if (tipoHab != null) {
                tipoHab.getHabitacionList().add(habitacion);
                tipoHab = em.merge(tipoHab);
            }
            for (Asignarhab asignarhabListAsignarhab : habitacion.getAsignarhabList()) {
                Habitacion oldHabitacionOfAsignarhabListAsignarhab = asignarhabListAsignarhab.getHabitacion();
                asignarhabListAsignarhab.setHabitacion(habitacion);
                asignarhabListAsignarhab = em.merge(asignarhabListAsignarhab);
                if (oldHabitacionOfAsignarhabListAsignarhab != null) {
                    oldHabitacionOfAsignarhabListAsignarhab.getAsignarhabList().remove(asignarhabListAsignarhab);
                    oldHabitacionOfAsignarhabListAsignarhab = em.merge(oldHabitacionOfAsignarhabListAsignarhab);
                }
            }
            for (Limpieza limpiezaListLimpieza : habitacion.getLimpiezaList()) {
                Habitacion oldHabitacionHabitacionOfLimpiezaListLimpieza = limpiezaListLimpieza.getHabitacionHabitacion();
                limpiezaListLimpieza.setHabitacionHabitacion(habitacion);
                limpiezaListLimpieza = em.merge(limpiezaListLimpieza);
                if (oldHabitacionHabitacionOfLimpiezaListLimpieza != null) {
                    oldHabitacionHabitacionOfLimpiezaListLimpieza.getLimpiezaList().remove(limpiezaListLimpieza);
                    oldHabitacionHabitacionOfLimpiezaListLimpieza = em.merge(oldHabitacionHabitacionOfLimpiezaListLimpieza);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findHabitacion(habitacion.getIdHabitacion()) != null) {
                throw new PreexistingEntityException("Habitacion " + habitacion + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Habitacion habitacion) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Habitacion persistentHabitacion = em.find(Habitacion.class, habitacion.getIdHabitacion());
            Estado estadoOld = persistentHabitacion.getEstado();
            Estado estadoNew = habitacion.getEstado();
            Tipohabitacion tipoHabOld = persistentHabitacion.getTipoHab();
            Tipohabitacion tipoHabNew = habitacion.getTipoHab();
            List<Asignarhab> asignarhabListOld = persistentHabitacion.getAsignarhabList();
            List<Asignarhab> asignarhabListNew = habitacion.getAsignarhabList();
            List<Limpieza> limpiezaListOld = persistentHabitacion.getLimpiezaList();
            List<Limpieza> limpiezaListNew = habitacion.getLimpiezaList();
            List<String> illegalOrphanMessages = null;
            for (Asignarhab asignarhabListOldAsignarhab : asignarhabListOld) {
                if (!asignarhabListNew.contains(asignarhabListOldAsignarhab)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Asignarhab " + asignarhabListOldAsignarhab + " since its habitacion field is not nullable.");
                }
            }
            for (Limpieza limpiezaListOldLimpieza : limpiezaListOld) {
                if (!limpiezaListNew.contains(limpiezaListOldLimpieza)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Limpieza " + limpiezaListOldLimpieza + " since its habitacionHabitacion field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (estadoNew != null) {
                estadoNew = em.getReference(estadoNew.getClass(), estadoNew.getIdEstado());
                habitacion.setEstado(estadoNew);
            }
            if (tipoHabNew != null) {
                tipoHabNew = em.getReference(tipoHabNew.getClass(), tipoHabNew.getIdTipo());
                habitacion.setTipoHab(tipoHabNew);
            }
            List<Asignarhab> attachedAsignarhabListNew = new ArrayList<Asignarhab>();
            for (Asignarhab asignarhabListNewAsignarhabToAttach : asignarhabListNew) {
                asignarhabListNewAsignarhabToAttach = em.getReference(asignarhabListNewAsignarhabToAttach.getClass(), asignarhabListNewAsignarhabToAttach.getAsignarhab());
                attachedAsignarhabListNew.add(asignarhabListNewAsignarhabToAttach);
            }
            asignarhabListNew = attachedAsignarhabListNew;
            habitacion.setAsignarhabList(asignarhabListNew);
            List<Limpieza> attachedLimpiezaListNew = new ArrayList<Limpieza>();
            for (Limpieza limpiezaListNewLimpiezaToAttach : limpiezaListNew) {
                limpiezaListNewLimpiezaToAttach = em.getReference(limpiezaListNewLimpiezaToAttach.getClass(), limpiezaListNewLimpiezaToAttach.getIdLimpieza());
                attachedLimpiezaListNew.add(limpiezaListNewLimpiezaToAttach);
            }
            limpiezaListNew = attachedLimpiezaListNew;
            habitacion.setLimpiezaList(limpiezaListNew);
            habitacion = em.merge(habitacion);
            if (estadoOld != null && !estadoOld.equals(estadoNew)) {
                estadoOld.getHabitacionList().remove(habitacion);
                estadoOld = em.merge(estadoOld);
            }
            if (estadoNew != null && !estadoNew.equals(estadoOld)) {
                estadoNew.getHabitacionList().add(habitacion);
                estadoNew = em.merge(estadoNew);
            }
            if (tipoHabOld != null && !tipoHabOld.equals(tipoHabNew)) {
                tipoHabOld.getHabitacionList().remove(habitacion);
                tipoHabOld = em.merge(tipoHabOld);
            }
            if (tipoHabNew != null && !tipoHabNew.equals(tipoHabOld)) {
                tipoHabNew.getHabitacionList().add(habitacion);
                tipoHabNew = em.merge(tipoHabNew);
            }
            for (Asignarhab asignarhabListNewAsignarhab : asignarhabListNew) {
                if (!asignarhabListOld.contains(asignarhabListNewAsignarhab)) {
                    Habitacion oldHabitacionOfAsignarhabListNewAsignarhab = asignarhabListNewAsignarhab.getHabitacion();
                    asignarhabListNewAsignarhab.setHabitacion(habitacion);
                    asignarhabListNewAsignarhab = em.merge(asignarhabListNewAsignarhab);
                    if (oldHabitacionOfAsignarhabListNewAsignarhab != null && !oldHabitacionOfAsignarhabListNewAsignarhab.equals(habitacion)) {
                        oldHabitacionOfAsignarhabListNewAsignarhab.getAsignarhabList().remove(asignarhabListNewAsignarhab);
                        oldHabitacionOfAsignarhabListNewAsignarhab = em.merge(oldHabitacionOfAsignarhabListNewAsignarhab);
                    }
                }
            }
            for (Limpieza limpiezaListNewLimpieza : limpiezaListNew) {
                if (!limpiezaListOld.contains(limpiezaListNewLimpieza)) {
                    Habitacion oldHabitacionHabitacionOfLimpiezaListNewLimpieza = limpiezaListNewLimpieza.getHabitacionHabitacion();
                    limpiezaListNewLimpieza.setHabitacionHabitacion(habitacion);
                    limpiezaListNewLimpieza = em.merge(limpiezaListNewLimpieza);
                    if (oldHabitacionHabitacionOfLimpiezaListNewLimpieza != null && !oldHabitacionHabitacionOfLimpiezaListNewLimpieza.equals(habitacion)) {
                        oldHabitacionHabitacionOfLimpiezaListNewLimpieza.getLimpiezaList().remove(limpiezaListNewLimpieza);
                        oldHabitacionHabitacionOfLimpiezaListNewLimpieza = em.merge(oldHabitacionHabitacionOfLimpiezaListNewLimpieza);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = habitacion.getIdHabitacion();
                if (findHabitacion(id) == null) {
                    throw new NonexistentEntityException("The habitacion with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Habitacion habitacion;
            try {
                habitacion = em.getReference(Habitacion.class, id);
                habitacion.getIdHabitacion();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The habitacion with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Asignarhab> asignarhabListOrphanCheck = habitacion.getAsignarhabList();
            for (Asignarhab asignarhabListOrphanCheckAsignarhab : asignarhabListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Habitacion (" + habitacion + ") cannot be destroyed since the Asignarhab " + asignarhabListOrphanCheckAsignarhab + " in its asignarhabList field has a non-nullable habitacion field.");
            }
            List<Limpieza> limpiezaListOrphanCheck = habitacion.getLimpiezaList();
            for (Limpieza limpiezaListOrphanCheckLimpieza : limpiezaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Habitacion (" + habitacion + ") cannot be destroyed since the Limpieza " + limpiezaListOrphanCheckLimpieza + " in its limpiezaList field has a non-nullable habitacionHabitacion field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Estado estado = habitacion.getEstado();
            if (estado != null) {
                estado.getHabitacionList().remove(habitacion);
                estado = em.merge(estado);
            }
            Tipohabitacion tipoHab = habitacion.getTipoHab();
            if (tipoHab != null) {
                tipoHab.getHabitacionList().remove(habitacion);
                tipoHab = em.merge(tipoHab);
            }
            em.remove(habitacion);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Habitacion> findHabitacionEntities() {
        return findHabitacionEntities(true, -1, -1);
    }

    public List<Habitacion> findHabitacionEntities(int maxResults, int firstResult) {
        return findHabitacionEntities(false, maxResults, firstResult);
    }

    private List<Habitacion> findHabitacionEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Habitacion.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Habitacion findHabitacion(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Habitacion.class, id);
        } finally {
            em.close();
        }
    }

    public int getHabitacionCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Habitacion> rt = cq.from(Habitacion.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
