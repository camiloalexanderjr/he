/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Dao.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Dto.Habitacion;
import Dto.Empleado;
import Dto.Limpieza;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Alexander
 */
public class LimpiezaJpaController implements Serializable {

    public LimpiezaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Limpieza limpieza) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Habitacion habitacionHabitacion = limpieza.getHabitacionHabitacion();
            if (habitacionHabitacion != null) {
                habitacionHabitacion = em.getReference(habitacionHabitacion.getClass(), habitacionHabitacion.getIdHabitacion());
                limpieza.setHabitacionHabitacion(habitacionHabitacion);
            }
            Empleado nombreEmpleado = limpieza.getNombreEmpleado();
            if (nombreEmpleado != null) {
                nombreEmpleado = em.getReference(nombreEmpleado.getClass(), nombreEmpleado.getNombre());
                limpieza.setNombreEmpleado(nombreEmpleado);
            }
            em.persist(limpieza);
            if (habitacionHabitacion != null) {
                habitacionHabitacion.getLimpiezaList().add(limpieza);
                habitacionHabitacion = em.merge(habitacionHabitacion);
            }
            if (nombreEmpleado != null) {
                nombreEmpleado.getLimpiezaList().add(limpieza);
                nombreEmpleado = em.merge(nombreEmpleado);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Limpieza limpieza) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Limpieza persistentLimpieza = em.find(Limpieza.class, limpieza.getIdLimpieza());
            Habitacion habitacionHabitacionOld = persistentLimpieza.getHabitacionHabitacion();
            Habitacion habitacionHabitacionNew = limpieza.getHabitacionHabitacion();
            Empleado nombreEmpleadoOld = persistentLimpieza.getNombreEmpleado();
            Empleado nombreEmpleadoNew = limpieza.getNombreEmpleado();
            if (habitacionHabitacionNew != null) {
                habitacionHabitacionNew = em.getReference(habitacionHabitacionNew.getClass(), habitacionHabitacionNew.getIdHabitacion());
                limpieza.setHabitacionHabitacion(habitacionHabitacionNew);
            }
            if (nombreEmpleadoNew != null) {
                nombreEmpleadoNew = em.getReference(nombreEmpleadoNew.getClass(), nombreEmpleadoNew.getNombre());
                limpieza.setNombreEmpleado(nombreEmpleadoNew);
            }
            limpieza = em.merge(limpieza);
            if (habitacionHabitacionOld != null && !habitacionHabitacionOld.equals(habitacionHabitacionNew)) {
                habitacionHabitacionOld.getLimpiezaList().remove(limpieza);
                habitacionHabitacionOld = em.merge(habitacionHabitacionOld);
            }
            if (habitacionHabitacionNew != null && !habitacionHabitacionNew.equals(habitacionHabitacionOld)) {
                habitacionHabitacionNew.getLimpiezaList().add(limpieza);
                habitacionHabitacionNew = em.merge(habitacionHabitacionNew);
            }
            if (nombreEmpleadoOld != null && !nombreEmpleadoOld.equals(nombreEmpleadoNew)) {
                nombreEmpleadoOld.getLimpiezaList().remove(limpieza);
                nombreEmpleadoOld = em.merge(nombreEmpleadoOld);
            }
            if (nombreEmpleadoNew != null && !nombreEmpleadoNew.equals(nombreEmpleadoOld)) {
                nombreEmpleadoNew.getLimpiezaList().add(limpieza);
                nombreEmpleadoNew = em.merge(nombreEmpleadoNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = limpieza.getIdLimpieza();
                if (findLimpieza(id) == null) {
                    throw new NonexistentEntityException("The limpieza with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Limpieza limpieza;
            try {
                limpieza = em.getReference(Limpieza.class, id);
                limpieza.getIdLimpieza();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The limpieza with id " + id + " no longer exists.", enfe);
            }
            Habitacion habitacionHabitacion = limpieza.getHabitacionHabitacion();
            if (habitacionHabitacion != null) {
                habitacionHabitacion.getLimpiezaList().remove(limpieza);
                habitacionHabitacion = em.merge(habitacionHabitacion);
            }
            Empleado nombreEmpleado = limpieza.getNombreEmpleado();
            if (nombreEmpleado != null) {
                nombreEmpleado.getLimpiezaList().remove(limpieza);
                nombreEmpleado = em.merge(nombreEmpleado);
            }
            em.remove(limpieza);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Limpieza> findLimpiezaEntities() {
        return findLimpiezaEntities(true, -1, -1);
    }

    public List<Limpieza> findLimpiezaEntities(int maxResults, int firstResult) {
        return findLimpiezaEntities(false, maxResults, firstResult);
    }

    private List<Limpieza> findLimpiezaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Limpieza.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Limpieza findLimpieza(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Limpieza.class, id);
        } finally {
            em.close();
        }
    }

    public int getLimpiezaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Limpieza> rt = cq.from(Limpieza.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
