/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Dao.exceptions.IllegalOrphanException;
import Dao.exceptions.NonexistentEntityException;
import Dao.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Dto.Guardarobjeto;
import Dto.Asignarhab;
import Dto.Cliente;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Alexander
 */
public class ClienteJpaController implements Serializable {

    public ClienteJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Cliente cliente) throws PreexistingEntityException, Exception {
        if (cliente.getAsignarhabList() == null) {
            cliente.setAsignarhabList(new ArrayList<Asignarhab>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Guardarobjeto objeto = cliente.getObjeto();
            if (objeto != null) {
                objeto = em.getReference(objeto.getClass(), objeto.getIdObjeto());
                cliente.setObjeto(objeto);
            }
            List<Asignarhab> attachedAsignarhabList = new ArrayList<Asignarhab>();
            for (Asignarhab asignarhabListAsignarhabToAttach : cliente.getAsignarhabList()) {
                asignarhabListAsignarhabToAttach = em.getReference(asignarhabListAsignarhabToAttach.getClass(), asignarhabListAsignarhabToAttach.getAsignarhab());
                attachedAsignarhabList.add(asignarhabListAsignarhabToAttach);
            }
            cliente.setAsignarhabList(attachedAsignarhabList);
            em.persist(cliente);
            if (objeto != null) {
                objeto.getClienteList().add(cliente);
                objeto = em.merge(objeto);
            }
            for (Asignarhab asignarhabListAsignarhab : cliente.getAsignarhabList()) {
                Cliente oldClienteOfAsignarhabListAsignarhab = asignarhabListAsignarhab.getCliente();
                asignarhabListAsignarhab.setCliente(cliente);
                asignarhabListAsignarhab = em.merge(asignarhabListAsignarhab);
                if (oldClienteOfAsignarhabListAsignarhab != null) {
                    oldClienteOfAsignarhabListAsignarhab.getAsignarhabList().remove(asignarhabListAsignarhab);
                    oldClienteOfAsignarhabListAsignarhab = em.merge(oldClienteOfAsignarhabListAsignarhab);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCliente(cliente.getCliente()) != null) {
                throw new PreexistingEntityException("Cliente " + cliente + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Cliente cliente) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cliente persistentCliente = em.find(Cliente.class, cliente.getCliente());
            Guardarobjeto objetoOld = persistentCliente.getObjeto();
            Guardarobjeto objetoNew = cliente.getObjeto();
            List<Asignarhab> asignarhabListOld = persistentCliente.getAsignarhabList();
            List<Asignarhab> asignarhabListNew = cliente.getAsignarhabList();
            List<String> illegalOrphanMessages = null;
            for (Asignarhab asignarhabListOldAsignarhab : asignarhabListOld) {
                if (!asignarhabListNew.contains(asignarhabListOldAsignarhab)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Asignarhab " + asignarhabListOldAsignarhab + " since its cliente field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (objetoNew != null) {
                objetoNew = em.getReference(objetoNew.getClass(), objetoNew.getIdObjeto());
                cliente.setObjeto(objetoNew);
            }
            List<Asignarhab> attachedAsignarhabListNew = new ArrayList<Asignarhab>();
            for (Asignarhab asignarhabListNewAsignarhabToAttach : asignarhabListNew) {
                asignarhabListNewAsignarhabToAttach = em.getReference(asignarhabListNewAsignarhabToAttach.getClass(), asignarhabListNewAsignarhabToAttach.getAsignarhab());
                attachedAsignarhabListNew.add(asignarhabListNewAsignarhabToAttach);
            }
            asignarhabListNew = attachedAsignarhabListNew;
            cliente.setAsignarhabList(asignarhabListNew);
            cliente = em.merge(cliente);
            if (objetoOld != null && !objetoOld.equals(objetoNew)) {
                objetoOld.getClienteList().remove(cliente);
                objetoOld = em.merge(objetoOld);
            }
            if (objetoNew != null && !objetoNew.equals(objetoOld)) {
                objetoNew.getClienteList().add(cliente);
                objetoNew = em.merge(objetoNew);
            }
            for (Asignarhab asignarhabListNewAsignarhab : asignarhabListNew) {
                if (!asignarhabListOld.contains(asignarhabListNewAsignarhab)) {
                    Cliente oldClienteOfAsignarhabListNewAsignarhab = asignarhabListNewAsignarhab.getCliente();
                    asignarhabListNewAsignarhab.setCliente(cliente);
                    asignarhabListNewAsignarhab = em.merge(asignarhabListNewAsignarhab);
                    if (oldClienteOfAsignarhabListNewAsignarhab != null && !oldClienteOfAsignarhabListNewAsignarhab.equals(cliente)) {
                        oldClienteOfAsignarhabListNewAsignarhab.getAsignarhabList().remove(asignarhabListNewAsignarhab);
                        oldClienteOfAsignarhabListNewAsignarhab = em.merge(oldClienteOfAsignarhabListNewAsignarhab);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = cliente.getCliente();
                if (findCliente(id) == null) {
                    throw new NonexistentEntityException("The cliente with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cliente cliente;
            try {
                cliente = em.getReference(Cliente.class, id);
                cliente.getCliente();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The cliente with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Asignarhab> asignarhabListOrphanCheck = cliente.getAsignarhabList();
            for (Asignarhab asignarhabListOrphanCheckAsignarhab : asignarhabListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Cliente (" + cliente + ") cannot be destroyed since the Asignarhab " + asignarhabListOrphanCheckAsignarhab + " in its asignarhabList field has a non-nullable cliente field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Guardarobjeto objeto = cliente.getObjeto();
            if (objeto != null) {
                objeto.getClienteList().remove(cliente);
                objeto = em.merge(objeto);
            }
            em.remove(cliente);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Cliente> findClienteEntities() {
        return findClienteEntities(true, -1, -1);
    }

    public List<Cliente> findClienteEntities(int maxResults, int firstResult) {
        return findClienteEntities(false, maxResults, firstResult);
    }

    private List<Cliente> findClienteEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Cliente.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Cliente findCliente(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Cliente.class, id);
        } finally {
            em.close();
        }
    }

    public int getClienteCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Cliente> rt = cq.from(Cliente.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
