/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Dao.exceptions.IllegalOrphanException;
import Dao.exceptions.NonexistentEntityException;
import Dao.exceptions.PreexistingEntityException;
import Dto.Empleado;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Dto.Limpieza;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Alexander
 */
public class EmpleadoJpaController implements Serializable {

    public EmpleadoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Empleado empleado) throws PreexistingEntityException, Exception {
        if (empleado.getLimpiezaList() == null) {
            empleado.setLimpiezaList(new ArrayList<Limpieza>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Limpieza> attachedLimpiezaList = new ArrayList<Limpieza>();
            for (Limpieza limpiezaListLimpiezaToAttach : empleado.getLimpiezaList()) {
                limpiezaListLimpiezaToAttach = em.getReference(limpiezaListLimpiezaToAttach.getClass(), limpiezaListLimpiezaToAttach.getIdLimpieza());
                attachedLimpiezaList.add(limpiezaListLimpiezaToAttach);
            }
            empleado.setLimpiezaList(attachedLimpiezaList);
            em.persist(empleado);
            for (Limpieza limpiezaListLimpieza : empleado.getLimpiezaList()) {
                Empleado oldNombreEmpleadoOfLimpiezaListLimpieza = limpiezaListLimpieza.getNombreEmpleado();
                limpiezaListLimpieza.setNombreEmpleado(empleado);
                limpiezaListLimpieza = em.merge(limpiezaListLimpieza);
                if (oldNombreEmpleadoOfLimpiezaListLimpieza != null) {
                    oldNombreEmpleadoOfLimpiezaListLimpieza.getLimpiezaList().remove(limpiezaListLimpieza);
                    oldNombreEmpleadoOfLimpiezaListLimpieza = em.merge(oldNombreEmpleadoOfLimpiezaListLimpieza);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findEmpleado(empleado.getNombre()) != null) {
                throw new PreexistingEntityException("Empleado " + empleado + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Empleado empleado) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Empleado persistentEmpleado = em.find(Empleado.class, empleado.getNombre());
            List<Limpieza> limpiezaListOld = persistentEmpleado.getLimpiezaList();
            List<Limpieza> limpiezaListNew = empleado.getLimpiezaList();
            List<String> illegalOrphanMessages = null;
            for (Limpieza limpiezaListOldLimpieza : limpiezaListOld) {
                if (!limpiezaListNew.contains(limpiezaListOldLimpieza)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Limpieza " + limpiezaListOldLimpieza + " since its nombreEmpleado field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Limpieza> attachedLimpiezaListNew = new ArrayList<Limpieza>();
            for (Limpieza limpiezaListNewLimpiezaToAttach : limpiezaListNew) {
                limpiezaListNewLimpiezaToAttach = em.getReference(limpiezaListNewLimpiezaToAttach.getClass(), limpiezaListNewLimpiezaToAttach.getIdLimpieza());
                attachedLimpiezaListNew.add(limpiezaListNewLimpiezaToAttach);
            }
            limpiezaListNew = attachedLimpiezaListNew;
            empleado.setLimpiezaList(limpiezaListNew);
            empleado = em.merge(empleado);
            for (Limpieza limpiezaListNewLimpieza : limpiezaListNew) {
                if (!limpiezaListOld.contains(limpiezaListNewLimpieza)) {
                    Empleado oldNombreEmpleadoOfLimpiezaListNewLimpieza = limpiezaListNewLimpieza.getNombreEmpleado();
                    limpiezaListNewLimpieza.setNombreEmpleado(empleado);
                    limpiezaListNewLimpieza = em.merge(limpiezaListNewLimpieza);
                    if (oldNombreEmpleadoOfLimpiezaListNewLimpieza != null && !oldNombreEmpleadoOfLimpiezaListNewLimpieza.equals(empleado)) {
                        oldNombreEmpleadoOfLimpiezaListNewLimpieza.getLimpiezaList().remove(limpiezaListNewLimpieza);
                        oldNombreEmpleadoOfLimpiezaListNewLimpieza = em.merge(oldNombreEmpleadoOfLimpiezaListNewLimpieza);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = empleado.getNombre();
                if (findEmpleado(id) == null) {
                    throw new NonexistentEntityException("The empleado with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Empleado empleado;
            try {
                empleado = em.getReference(Empleado.class, id);
                empleado.getNombre();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The empleado with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Limpieza> limpiezaListOrphanCheck = empleado.getLimpiezaList();
            for (Limpieza limpiezaListOrphanCheckLimpieza : limpiezaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Empleado (" + empleado + ") cannot be destroyed since the Limpieza " + limpiezaListOrphanCheckLimpieza + " in its limpiezaList field has a non-nullable nombreEmpleado field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(empleado);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Empleado> findEmpleadoEntities() {
        return findEmpleadoEntities(true, -1, -1);
    }

    public List<Empleado> findEmpleadoEntities(int maxResults, int firstResult) {
        return findEmpleadoEntities(false, maxResults, firstResult);
    }

    private List<Empleado> findEmpleadoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Empleado.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Empleado findEmpleado(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Empleado.class, id);
        } finally {
            em.close();
        }
    }

    public int getEmpleadoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Empleado> rt = cq.from(Empleado.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
