/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Dao.exceptions.IllegalOrphanException;
import Dao.exceptions.NonexistentEntityException;
import Dto.Cafeteria;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Dto.Usuario;
import Dto.Caja;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Alexander
 */
public class CafeteriaJpaController implements Serializable {

    public CafeteriaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Cafeteria cafeteria) {
        if (cafeteria.getCajaList() == null) {
            cafeteria.setCajaList(new ArrayList<Caja>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Usuario usuarioUsuario = cafeteria.getUsuarioUsuario();
            if (usuarioUsuario != null) {
                usuarioUsuario = em.getReference(usuarioUsuario.getClass(), usuarioUsuario.getUsuario());
                cafeteria.setUsuarioUsuario(usuarioUsuario);
            }
            List<Caja> attachedCajaList = new ArrayList<Caja>();
            for (Caja cajaListCajaToAttach : cafeteria.getCajaList()) {
                cajaListCajaToAttach = em.getReference(cajaListCajaToAttach.getClass(), cajaListCajaToAttach.getIdEstado());
                attachedCajaList.add(cajaListCajaToAttach);
            }
            cafeteria.setCajaList(attachedCajaList);
            em.persist(cafeteria);
            if (usuarioUsuario != null) {
                usuarioUsuario.getCafeteriaList().add(cafeteria);
                usuarioUsuario = em.merge(usuarioUsuario);
            }
            for (Caja cajaListCaja : cafeteria.getCajaList()) {
                Cafeteria oldCafeteriaCafeteriaOfCajaListCaja = cajaListCaja.getCafeteriaCafeteria();
                cajaListCaja.setCafeteriaCafeteria(cafeteria);
                cajaListCaja = em.merge(cajaListCaja);
                if (oldCafeteriaCafeteriaOfCajaListCaja != null) {
                    oldCafeteriaCafeteriaOfCajaListCaja.getCajaList().remove(cajaListCaja);
                    oldCafeteriaCafeteriaOfCajaListCaja = em.merge(oldCafeteriaCafeteriaOfCajaListCaja);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Cafeteria cafeteria) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cafeteria persistentCafeteria = em.find(Cafeteria.class, cafeteria.getIdCompra());
            Usuario usuarioUsuarioOld = persistentCafeteria.getUsuarioUsuario();
            Usuario usuarioUsuarioNew = cafeteria.getUsuarioUsuario();
            List<Caja> cajaListOld = persistentCafeteria.getCajaList();
            List<Caja> cajaListNew = cafeteria.getCajaList();
            List<String> illegalOrphanMessages = null;
            for (Caja cajaListOldCaja : cajaListOld) {
                if (!cajaListNew.contains(cajaListOldCaja)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Caja " + cajaListOldCaja + " since its cafeteriaCafeteria field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (usuarioUsuarioNew != null) {
                usuarioUsuarioNew = em.getReference(usuarioUsuarioNew.getClass(), usuarioUsuarioNew.getUsuario());
                cafeteria.setUsuarioUsuario(usuarioUsuarioNew);
            }
            List<Caja> attachedCajaListNew = new ArrayList<Caja>();
            for (Caja cajaListNewCajaToAttach : cajaListNew) {
                cajaListNewCajaToAttach = em.getReference(cajaListNewCajaToAttach.getClass(), cajaListNewCajaToAttach.getIdEstado());
                attachedCajaListNew.add(cajaListNewCajaToAttach);
            }
            cajaListNew = attachedCajaListNew;
            cafeteria.setCajaList(cajaListNew);
            cafeteria = em.merge(cafeteria);
            if (usuarioUsuarioOld != null && !usuarioUsuarioOld.equals(usuarioUsuarioNew)) {
                usuarioUsuarioOld.getCafeteriaList().remove(cafeteria);
                usuarioUsuarioOld = em.merge(usuarioUsuarioOld);
            }
            if (usuarioUsuarioNew != null && !usuarioUsuarioNew.equals(usuarioUsuarioOld)) {
                usuarioUsuarioNew.getCafeteriaList().add(cafeteria);
                usuarioUsuarioNew = em.merge(usuarioUsuarioNew);
            }
            for (Caja cajaListNewCaja : cajaListNew) {
                if (!cajaListOld.contains(cajaListNewCaja)) {
                    Cafeteria oldCafeteriaCafeteriaOfCajaListNewCaja = cajaListNewCaja.getCafeteriaCafeteria();
                    cajaListNewCaja.setCafeteriaCafeteria(cafeteria);
                    cajaListNewCaja = em.merge(cajaListNewCaja);
                    if (oldCafeteriaCafeteriaOfCajaListNewCaja != null && !oldCafeteriaCafeteriaOfCajaListNewCaja.equals(cafeteria)) {
                        oldCafeteriaCafeteriaOfCajaListNewCaja.getCajaList().remove(cajaListNewCaja);
                        oldCafeteriaCafeteriaOfCajaListNewCaja = em.merge(oldCafeteriaCafeteriaOfCajaListNewCaja);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = cafeteria.getIdCompra();
                if (findCafeteria(id) == null) {
                    throw new NonexistentEntityException("The cafeteria with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cafeteria cafeteria;
            try {
                cafeteria = em.getReference(Cafeteria.class, id);
                cafeteria.getIdCompra();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The cafeteria with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Caja> cajaListOrphanCheck = cafeteria.getCajaList();
            for (Caja cajaListOrphanCheckCaja : cajaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Cafeteria (" + cafeteria + ") cannot be destroyed since the Caja " + cajaListOrphanCheckCaja + " in its cajaList field has a non-nullable cafeteriaCafeteria field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Usuario usuarioUsuario = cafeteria.getUsuarioUsuario();
            if (usuarioUsuario != null) {
                usuarioUsuario.getCafeteriaList().remove(cafeteria);
                usuarioUsuario = em.merge(usuarioUsuario);
            }
            em.remove(cafeteria);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Cafeteria> findCafeteriaEntities() {
        return findCafeteriaEntities(true, -1, -1);
    }

    public List<Cafeteria> findCafeteriaEntities(int maxResults, int firstResult) {
        return findCafeteriaEntities(false, maxResults, firstResult);
    }

    private List<Cafeteria> findCafeteriaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Cafeteria.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Cafeteria findCafeteria(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Cafeteria.class, id);
        } finally {
            em.close();
        }
    }

    public int getCafeteriaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Cafeteria> rt = cq.from(Cafeteria.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
