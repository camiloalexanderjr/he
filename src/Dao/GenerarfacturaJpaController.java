/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Dao.exceptions.IllegalOrphanException;
import Dao.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Dto.Usuario;
import Dto.Asignarhab;
import Dto.Caja;
import Dto.Generarfactura;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Alexander
 */
public class GenerarfacturaJpaController implements Serializable {

    public GenerarfacturaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Generarfactura generarfactura) {
        if (generarfactura.getCajaList() == null) {
            generarfactura.setCajaList(new ArrayList<Caja>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Usuario usuarioUsuario = generarfactura.getUsuarioUsuario();
            if (usuarioUsuario != null) {
                usuarioUsuario = em.getReference(usuarioUsuario.getClass(), usuarioUsuario.getUsuario());
                generarfactura.setUsuarioUsuario(usuarioUsuario);
            }
            Asignarhab asignarHab = generarfactura.getAsignarHab();
            if (asignarHab != null) {
                asignarHab = em.getReference(asignarHab.getClass(), asignarHab.getAsignarhab());
                generarfactura.setAsignarHab(asignarHab);
            }
            List<Caja> attachedCajaList = new ArrayList<Caja>();
            for (Caja cajaListCajaToAttach : generarfactura.getCajaList()) {
                cajaListCajaToAttach = em.getReference(cajaListCajaToAttach.getClass(), cajaListCajaToAttach.getIdEstado());
                attachedCajaList.add(cajaListCajaToAttach);
            }
            generarfactura.setCajaList(attachedCajaList);
            em.persist(generarfactura);
            if (usuarioUsuario != null) {
                usuarioUsuario.getGenerarfacturaList().add(generarfactura);
                usuarioUsuario = em.merge(usuarioUsuario);
            }
            if (asignarHab != null) {
                asignarHab.getGenerarfacturaList().add(generarfactura);
                asignarHab = em.merge(asignarHab);
            }
            for (Caja cajaListCaja : generarfactura.getCajaList()) {
                Generarfactura oldFacturaFacturaOfCajaListCaja = cajaListCaja.getFacturaFactura();
                cajaListCaja.setFacturaFactura(generarfactura);
                cajaListCaja = em.merge(cajaListCaja);
                if (oldFacturaFacturaOfCajaListCaja != null) {
                    oldFacturaFacturaOfCajaListCaja.getCajaList().remove(cajaListCaja);
                    oldFacturaFacturaOfCajaListCaja = em.merge(oldFacturaFacturaOfCajaListCaja);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Generarfactura generarfactura) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Generarfactura persistentGenerarfactura = em.find(Generarfactura.class, generarfactura.getIdFactura());
            Usuario usuarioUsuarioOld = persistentGenerarfactura.getUsuarioUsuario();
            Usuario usuarioUsuarioNew = generarfactura.getUsuarioUsuario();
            Asignarhab asignarHabOld = persistentGenerarfactura.getAsignarHab();
            Asignarhab asignarHabNew = generarfactura.getAsignarHab();
            List<Caja> cajaListOld = persistentGenerarfactura.getCajaList();
            List<Caja> cajaListNew = generarfactura.getCajaList();
            List<String> illegalOrphanMessages = null;
            for (Caja cajaListOldCaja : cajaListOld) {
                if (!cajaListNew.contains(cajaListOldCaja)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Caja " + cajaListOldCaja + " since its facturaFactura field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (usuarioUsuarioNew != null) {
                usuarioUsuarioNew = em.getReference(usuarioUsuarioNew.getClass(), usuarioUsuarioNew.getUsuario());
                generarfactura.setUsuarioUsuario(usuarioUsuarioNew);
            }
            if (asignarHabNew != null) {
                asignarHabNew = em.getReference(asignarHabNew.getClass(), asignarHabNew.getAsignarhab());
                generarfactura.setAsignarHab(asignarHabNew);
            }
            List<Caja> attachedCajaListNew = new ArrayList<Caja>();
            for (Caja cajaListNewCajaToAttach : cajaListNew) {
                cajaListNewCajaToAttach = em.getReference(cajaListNewCajaToAttach.getClass(), cajaListNewCajaToAttach.getIdEstado());
                attachedCajaListNew.add(cajaListNewCajaToAttach);
            }
            cajaListNew = attachedCajaListNew;
            generarfactura.setCajaList(cajaListNew);
            generarfactura = em.merge(generarfactura);
            if (usuarioUsuarioOld != null && !usuarioUsuarioOld.equals(usuarioUsuarioNew)) {
                usuarioUsuarioOld.getGenerarfacturaList().remove(generarfactura);
                usuarioUsuarioOld = em.merge(usuarioUsuarioOld);
            }
            if (usuarioUsuarioNew != null && !usuarioUsuarioNew.equals(usuarioUsuarioOld)) {
                usuarioUsuarioNew.getGenerarfacturaList().add(generarfactura);
                usuarioUsuarioNew = em.merge(usuarioUsuarioNew);
            }
            if (asignarHabOld != null && !asignarHabOld.equals(asignarHabNew)) {
                asignarHabOld.getGenerarfacturaList().remove(generarfactura);
                asignarHabOld = em.merge(asignarHabOld);
            }
            if (asignarHabNew != null && !asignarHabNew.equals(asignarHabOld)) {
                asignarHabNew.getGenerarfacturaList().add(generarfactura);
                asignarHabNew = em.merge(asignarHabNew);
            }
            for (Caja cajaListNewCaja : cajaListNew) {
                if (!cajaListOld.contains(cajaListNewCaja)) {
                    Generarfactura oldFacturaFacturaOfCajaListNewCaja = cajaListNewCaja.getFacturaFactura();
                    cajaListNewCaja.setFacturaFactura(generarfactura);
                    cajaListNewCaja = em.merge(cajaListNewCaja);
                    if (oldFacturaFacturaOfCajaListNewCaja != null && !oldFacturaFacturaOfCajaListNewCaja.equals(generarfactura)) {
                        oldFacturaFacturaOfCajaListNewCaja.getCajaList().remove(cajaListNewCaja);
                        oldFacturaFacturaOfCajaListNewCaja = em.merge(oldFacturaFacturaOfCajaListNewCaja);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = generarfactura.getIdFactura();
                if (findGenerarfactura(id) == null) {
                    throw new NonexistentEntityException("The generarfactura with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Generarfactura generarfactura;
            try {
                generarfactura = em.getReference(Generarfactura.class, id);
                generarfactura.getIdFactura();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The generarfactura with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Caja> cajaListOrphanCheck = generarfactura.getCajaList();
            for (Caja cajaListOrphanCheckCaja : cajaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Generarfactura (" + generarfactura + ") cannot be destroyed since the Caja " + cajaListOrphanCheckCaja + " in its cajaList field has a non-nullable facturaFactura field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Usuario usuarioUsuario = generarfactura.getUsuarioUsuario();
            if (usuarioUsuario != null) {
                usuarioUsuario.getGenerarfacturaList().remove(generarfactura);
                usuarioUsuario = em.merge(usuarioUsuario);
            }
            Asignarhab asignarHab = generarfactura.getAsignarHab();
            if (asignarHab != null) {
                asignarHab.getGenerarfacturaList().remove(generarfactura);
                asignarHab = em.merge(asignarHab);
            }
            em.remove(generarfactura);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Generarfactura> findGenerarfacturaEntities() {
        return findGenerarfacturaEntities(true, -1, -1);
    }

    public List<Generarfactura> findGenerarfacturaEntities(int maxResults, int firstResult) {
        return findGenerarfacturaEntities(false, maxResults, firstResult);
    }

    private List<Generarfactura> findGenerarfacturaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Generarfactura.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Generarfactura findGenerarfactura(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Generarfactura.class, id);
        } finally {
            em.close();
        }
    }

    public int getGenerarfacturaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Generarfactura> rt = cq.from(Generarfactura.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
