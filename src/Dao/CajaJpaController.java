/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Dao.exceptions.NonexistentEntityException;
import Dao.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Dto.Cafeteria;
import Dto.Caja;
import Dto.Generarfactura;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Alexander
 */
public class CajaJpaController implements Serializable {

    public CajaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Caja caja) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cafeteria cafeteriaCafeteria = caja.getCafeteriaCafeteria();
            if (cafeteriaCafeteria != null) {
                cafeteriaCafeteria = em.getReference(cafeteriaCafeteria.getClass(), cafeteriaCafeteria.getIdCompra());
                caja.setCafeteriaCafeteria(cafeteriaCafeteria);
            }
            Generarfactura facturaFactura = caja.getFacturaFactura();
            if (facturaFactura != null) {
                facturaFactura = em.getReference(facturaFactura.getClass(), facturaFactura.getIdFactura());
                caja.setFacturaFactura(facturaFactura);
            }
            em.persist(caja);
            if (cafeteriaCafeteria != null) {
                cafeteriaCafeteria.getCajaList().add(caja);
                cafeteriaCafeteria = em.merge(cafeteriaCafeteria);
            }
            if (facturaFactura != null) {
                facturaFactura.getCajaList().add(caja);
                facturaFactura = em.merge(facturaFactura);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCaja(caja.getIdEstado()) != null) {
                throw new PreexistingEntityException("Caja " + caja + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Caja caja) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Caja persistentCaja = em.find(Caja.class, caja.getIdEstado());
            Cafeteria cafeteriaCafeteriaOld = persistentCaja.getCafeteriaCafeteria();
            Cafeteria cafeteriaCafeteriaNew = caja.getCafeteriaCafeteria();
            Generarfactura facturaFacturaOld = persistentCaja.getFacturaFactura();
            Generarfactura facturaFacturaNew = caja.getFacturaFactura();
            if (cafeteriaCafeteriaNew != null) {
                cafeteriaCafeteriaNew = em.getReference(cafeteriaCafeteriaNew.getClass(), cafeteriaCafeteriaNew.getIdCompra());
                caja.setCafeteriaCafeteria(cafeteriaCafeteriaNew);
            }
            if (facturaFacturaNew != null) {
                facturaFacturaNew = em.getReference(facturaFacturaNew.getClass(), facturaFacturaNew.getIdFactura());
                caja.setFacturaFactura(facturaFacturaNew);
            }
            caja = em.merge(caja);
            if (cafeteriaCafeteriaOld != null && !cafeteriaCafeteriaOld.equals(cafeteriaCafeteriaNew)) {
                cafeteriaCafeteriaOld.getCajaList().remove(caja);
                cafeteriaCafeteriaOld = em.merge(cafeteriaCafeteriaOld);
            }
            if (cafeteriaCafeteriaNew != null && !cafeteriaCafeteriaNew.equals(cafeteriaCafeteriaOld)) {
                cafeteriaCafeteriaNew.getCajaList().add(caja);
                cafeteriaCafeteriaNew = em.merge(cafeteriaCafeteriaNew);
            }
            if (facturaFacturaOld != null && !facturaFacturaOld.equals(facturaFacturaNew)) {
                facturaFacturaOld.getCajaList().remove(caja);
                facturaFacturaOld = em.merge(facturaFacturaOld);
            }
            if (facturaFacturaNew != null && !facturaFacturaNew.equals(facturaFacturaOld)) {
                facturaFacturaNew.getCajaList().add(caja);
                facturaFacturaNew = em.merge(facturaFacturaNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = caja.getIdEstado();
                if (findCaja(id) == null) {
                    throw new NonexistentEntityException("The caja with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Caja caja;
            try {
                caja = em.getReference(Caja.class, id);
                caja.getIdEstado();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The caja with id " + id + " no longer exists.", enfe);
            }
            Cafeteria cafeteriaCafeteria = caja.getCafeteriaCafeteria();
            if (cafeteriaCafeteria != null) {
                cafeteriaCafeteria.getCajaList().remove(caja);
                cafeteriaCafeteria = em.merge(cafeteriaCafeteria);
            }
            Generarfactura facturaFactura = caja.getFacturaFactura();
            if (facturaFactura != null) {
                facturaFactura.getCajaList().remove(caja);
                facturaFactura = em.merge(facturaFactura);
            }
            em.remove(caja);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Caja> findCajaEntities() {
        return findCajaEntities(true, -1, -1);
    }

    public List<Caja> findCajaEntities(int maxResults, int firstResult) {
        return findCajaEntities(false, maxResults, firstResult);
    }

    private List<Caja> findCajaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Caja.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Caja findCaja(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Caja.class, id);
        } finally {
            em.close();
        }
    }

    public int getCajaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Caja> rt = cq.from(Caja.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
