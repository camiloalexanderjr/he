/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Dao.exceptions.IllegalOrphanException;
import Dao.exceptions.NonexistentEntityException;
import Dto.Asignarhab;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Dto.Habitacion;
import Dto.Cliente;
import Dto.Usuario;
import Dto.Generarfactura;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Alexander
 */
public class AsignarhabJpaController implements Serializable {

    public AsignarhabJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Asignarhab asignarhab) {
        if (asignarhab.getGenerarfacturaList() == null) {
            asignarhab.setGenerarfacturaList(new ArrayList<Generarfactura>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Habitacion habitacion = asignarhab.getHabitacion();
            if (habitacion != null) {
                habitacion = em.getReference(habitacion.getClass(), habitacion.getIdHabitacion());
                asignarhab.setHabitacion(habitacion);
            }
            Cliente cliente = asignarhab.getCliente();
            if (cliente != null) {
                cliente = em.getReference(cliente.getClass(), cliente.getCliente());
                asignarhab.setCliente(cliente);
            }
            Usuario usuarioUsuario = asignarhab.getUsuarioUsuario();
            if (usuarioUsuario != null) {
                usuarioUsuario = em.getReference(usuarioUsuario.getClass(), usuarioUsuario.getUsuario());
                asignarhab.setUsuarioUsuario(usuarioUsuario);
            }
            List<Generarfactura> attachedGenerarfacturaList = new ArrayList<Generarfactura>();
            for (Generarfactura generarfacturaListGenerarfacturaToAttach : asignarhab.getGenerarfacturaList()) {
                generarfacturaListGenerarfacturaToAttach = em.getReference(generarfacturaListGenerarfacturaToAttach.getClass(), generarfacturaListGenerarfacturaToAttach.getIdFactura());
                attachedGenerarfacturaList.add(generarfacturaListGenerarfacturaToAttach);
            }
            asignarhab.setGenerarfacturaList(attachedGenerarfacturaList);
            em.persist(asignarhab);
            if (habitacion != null) {
                habitacion.getAsignarhabList().add(asignarhab);
                habitacion = em.merge(habitacion);
            }
            if (cliente != null) {
                cliente.getAsignarhabList().add(asignarhab);
                cliente = em.merge(cliente);
            }
            if (usuarioUsuario != null) {
                usuarioUsuario.getAsignarhabList().add(asignarhab);
                usuarioUsuario = em.merge(usuarioUsuario);
            }
            for (Generarfactura generarfacturaListGenerarfactura : asignarhab.getGenerarfacturaList()) {
                Asignarhab oldAsignarHabOfGenerarfacturaListGenerarfactura = generarfacturaListGenerarfactura.getAsignarHab();
                generarfacturaListGenerarfactura.setAsignarHab(asignarhab);
                generarfacturaListGenerarfactura = em.merge(generarfacturaListGenerarfactura);
                if (oldAsignarHabOfGenerarfacturaListGenerarfactura != null) {
                    oldAsignarHabOfGenerarfacturaListGenerarfactura.getGenerarfacturaList().remove(generarfacturaListGenerarfactura);
                    oldAsignarHabOfGenerarfacturaListGenerarfactura = em.merge(oldAsignarHabOfGenerarfacturaListGenerarfactura);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Asignarhab asignarhab) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Asignarhab persistentAsignarhab = em.find(Asignarhab.class, asignarhab.getAsignarhab());
            Habitacion habitacionOld = persistentAsignarhab.getHabitacion();
            Habitacion habitacionNew = asignarhab.getHabitacion();
            Cliente clienteOld = persistentAsignarhab.getCliente();
            Cliente clienteNew = asignarhab.getCliente();
            Usuario usuarioUsuarioOld = persistentAsignarhab.getUsuarioUsuario();
            Usuario usuarioUsuarioNew = asignarhab.getUsuarioUsuario();
            List<Generarfactura> generarfacturaListOld = persistentAsignarhab.getGenerarfacturaList();
            List<Generarfactura> generarfacturaListNew = asignarhab.getGenerarfacturaList();
            List<String> illegalOrphanMessages = null;
            for (Generarfactura generarfacturaListOldGenerarfactura : generarfacturaListOld) {
                if (!generarfacturaListNew.contains(generarfacturaListOldGenerarfactura)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Generarfactura " + generarfacturaListOldGenerarfactura + " since its asignarHab field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (habitacionNew != null) {
                habitacionNew = em.getReference(habitacionNew.getClass(), habitacionNew.getIdHabitacion());
                asignarhab.setHabitacion(habitacionNew);
            }
            if (clienteNew != null) {
                clienteNew = em.getReference(clienteNew.getClass(), clienteNew.getCliente());
                asignarhab.setCliente(clienteNew);
            }
            if (usuarioUsuarioNew != null) {
                usuarioUsuarioNew = em.getReference(usuarioUsuarioNew.getClass(), usuarioUsuarioNew.getUsuario());
                asignarhab.setUsuarioUsuario(usuarioUsuarioNew);
            }
            List<Generarfactura> attachedGenerarfacturaListNew = new ArrayList<Generarfactura>();
            for (Generarfactura generarfacturaListNewGenerarfacturaToAttach : generarfacturaListNew) {
                generarfacturaListNewGenerarfacturaToAttach = em.getReference(generarfacturaListNewGenerarfacturaToAttach.getClass(), generarfacturaListNewGenerarfacturaToAttach.getIdFactura());
                attachedGenerarfacturaListNew.add(generarfacturaListNewGenerarfacturaToAttach);
            }
            generarfacturaListNew = attachedGenerarfacturaListNew;
            asignarhab.setGenerarfacturaList(generarfacturaListNew);
            asignarhab = em.merge(asignarhab);
            if (habitacionOld != null && !habitacionOld.equals(habitacionNew)) {
                habitacionOld.getAsignarhabList().remove(asignarhab);
                habitacionOld = em.merge(habitacionOld);
            }
            if (habitacionNew != null && !habitacionNew.equals(habitacionOld)) {
                habitacionNew.getAsignarhabList().add(asignarhab);
                habitacionNew = em.merge(habitacionNew);
            }
            if (clienteOld != null && !clienteOld.equals(clienteNew)) {
                clienteOld.getAsignarhabList().remove(asignarhab);
                clienteOld = em.merge(clienteOld);
            }
            if (clienteNew != null && !clienteNew.equals(clienteOld)) {
                clienteNew.getAsignarhabList().add(asignarhab);
                clienteNew = em.merge(clienteNew);
            }
            if (usuarioUsuarioOld != null && !usuarioUsuarioOld.equals(usuarioUsuarioNew)) {
                usuarioUsuarioOld.getAsignarhabList().remove(asignarhab);
                usuarioUsuarioOld = em.merge(usuarioUsuarioOld);
            }
            if (usuarioUsuarioNew != null && !usuarioUsuarioNew.equals(usuarioUsuarioOld)) {
                usuarioUsuarioNew.getAsignarhabList().add(asignarhab);
                usuarioUsuarioNew = em.merge(usuarioUsuarioNew);
            }
            for (Generarfactura generarfacturaListNewGenerarfactura : generarfacturaListNew) {
                if (!generarfacturaListOld.contains(generarfacturaListNewGenerarfactura)) {
                    Asignarhab oldAsignarHabOfGenerarfacturaListNewGenerarfactura = generarfacturaListNewGenerarfactura.getAsignarHab();
                    generarfacturaListNewGenerarfactura.setAsignarHab(asignarhab);
                    generarfacturaListNewGenerarfactura = em.merge(generarfacturaListNewGenerarfactura);
                    if (oldAsignarHabOfGenerarfacturaListNewGenerarfactura != null && !oldAsignarHabOfGenerarfacturaListNewGenerarfactura.equals(asignarhab)) {
                        oldAsignarHabOfGenerarfacturaListNewGenerarfactura.getGenerarfacturaList().remove(generarfacturaListNewGenerarfactura);
                        oldAsignarHabOfGenerarfacturaListNewGenerarfactura = em.merge(oldAsignarHabOfGenerarfacturaListNewGenerarfactura);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = asignarhab.getAsignarhab();
                if (findAsignarhab(id) == null) {
                    throw new NonexistentEntityException("The asignarhab with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Asignarhab asignarhab;
            try {
                asignarhab = em.getReference(Asignarhab.class, id);
                asignarhab.getAsignarhab();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The asignarhab with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Generarfactura> generarfacturaListOrphanCheck = asignarhab.getGenerarfacturaList();
            for (Generarfactura generarfacturaListOrphanCheckGenerarfactura : generarfacturaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Asignarhab (" + asignarhab + ") cannot be destroyed since the Generarfactura " + generarfacturaListOrphanCheckGenerarfactura + " in its generarfacturaList field has a non-nullable asignarHab field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Habitacion habitacion = asignarhab.getHabitacion();
            if (habitacion != null) {
                habitacion.getAsignarhabList().remove(asignarhab);
                habitacion = em.merge(habitacion);
            }
            Cliente cliente = asignarhab.getCliente();
            if (cliente != null) {
                cliente.getAsignarhabList().remove(asignarhab);
                cliente = em.merge(cliente);
            }
            Usuario usuarioUsuario = asignarhab.getUsuarioUsuario();
            if (usuarioUsuario != null) {
                usuarioUsuario.getAsignarhabList().remove(asignarhab);
                usuarioUsuario = em.merge(usuarioUsuario);
            }
            em.remove(asignarhab);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Asignarhab> findAsignarhabEntities() {
        return findAsignarhabEntities(true, -1, -1);
    }

    public List<Asignarhab> findAsignarhabEntities(int maxResults, int firstResult) {
        return findAsignarhabEntities(false, maxResults, firstResult);
    }

    private List<Asignarhab> findAsignarhabEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Asignarhab.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Asignarhab findAsignarhab(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Asignarhab.class, id);
        } finally {
            em.close();
        }
    }

    public int getAsignarhabCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Asignarhab> rt = cq.from(Asignarhab.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
