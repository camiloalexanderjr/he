/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Dao.exceptions.IllegalOrphanException;
import Dao.exceptions.NonexistentEntityException;
import Dao.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Dto.Asignarhab;
import java.util.ArrayList;
import java.util.List;
import Dto.Cafeteria;
import Dto.Generarfactura;
import Dto.Reserva;
import Dto.Usuario;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Alexander
 */
public class UsuarioJpaController implements Serializable {

    public UsuarioJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Usuario usuario) throws PreexistingEntityException, Exception {
        if (usuario.getAsignarhabList() == null) {
            usuario.setAsignarhabList(new ArrayList<Asignarhab>());
        }
        if (usuario.getCafeteriaList() == null) {
            usuario.setCafeteriaList(new ArrayList<Cafeteria>());
        }
        if (usuario.getGenerarfacturaList() == null) {
            usuario.setGenerarfacturaList(new ArrayList<Generarfactura>());
        }
        if (usuario.getReservaList() == null) {
            usuario.setReservaList(new ArrayList<Reserva>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Asignarhab> attachedAsignarhabList = new ArrayList<Asignarhab>();
            for (Asignarhab asignarhabListAsignarhabToAttach : usuario.getAsignarhabList()) {
                asignarhabListAsignarhabToAttach = em.getReference(asignarhabListAsignarhabToAttach.getClass(), asignarhabListAsignarhabToAttach.getAsignarhab());
                attachedAsignarhabList.add(asignarhabListAsignarhabToAttach);
            }
            usuario.setAsignarhabList(attachedAsignarhabList);
            List<Cafeteria> attachedCafeteriaList = new ArrayList<Cafeteria>();
            for (Cafeteria cafeteriaListCafeteriaToAttach : usuario.getCafeteriaList()) {
                cafeteriaListCafeteriaToAttach = em.getReference(cafeteriaListCafeteriaToAttach.getClass(), cafeteriaListCafeteriaToAttach.getIdCompra());
                attachedCafeteriaList.add(cafeteriaListCafeteriaToAttach);
            }
            usuario.setCafeteriaList(attachedCafeteriaList);
            List<Generarfactura> attachedGenerarfacturaList = new ArrayList<Generarfactura>();
            for (Generarfactura generarfacturaListGenerarfacturaToAttach : usuario.getGenerarfacturaList()) {
                generarfacturaListGenerarfacturaToAttach = em.getReference(generarfacturaListGenerarfacturaToAttach.getClass(), generarfacturaListGenerarfacturaToAttach.getIdFactura());
                attachedGenerarfacturaList.add(generarfacturaListGenerarfacturaToAttach);
            }
            usuario.setGenerarfacturaList(attachedGenerarfacturaList);
            List<Reserva> attachedReservaList = new ArrayList<Reserva>();
            for (Reserva reservaListReservaToAttach : usuario.getReservaList()) {
                reservaListReservaToAttach = em.getReference(reservaListReservaToAttach.getClass(), reservaListReservaToAttach.getIdReserva());
                attachedReservaList.add(reservaListReservaToAttach);
            }
            usuario.setReservaList(attachedReservaList);
            em.persist(usuario);
            for (Asignarhab asignarhabListAsignarhab : usuario.getAsignarhabList()) {
                Usuario oldUsuarioUsuarioOfAsignarhabListAsignarhab = asignarhabListAsignarhab.getUsuarioUsuario();
                asignarhabListAsignarhab.setUsuarioUsuario(usuario);
                asignarhabListAsignarhab = em.merge(asignarhabListAsignarhab);
                if (oldUsuarioUsuarioOfAsignarhabListAsignarhab != null) {
                    oldUsuarioUsuarioOfAsignarhabListAsignarhab.getAsignarhabList().remove(asignarhabListAsignarhab);
                    oldUsuarioUsuarioOfAsignarhabListAsignarhab = em.merge(oldUsuarioUsuarioOfAsignarhabListAsignarhab);
                }
            }
            for (Cafeteria cafeteriaListCafeteria : usuario.getCafeteriaList()) {
                Usuario oldUsuarioUsuarioOfCafeteriaListCafeteria = cafeteriaListCafeteria.getUsuarioUsuario();
                cafeteriaListCafeteria.setUsuarioUsuario(usuario);
                cafeteriaListCafeteria = em.merge(cafeteriaListCafeteria);
                if (oldUsuarioUsuarioOfCafeteriaListCafeteria != null) {
                    oldUsuarioUsuarioOfCafeteriaListCafeteria.getCafeteriaList().remove(cafeteriaListCafeteria);
                    oldUsuarioUsuarioOfCafeteriaListCafeteria = em.merge(oldUsuarioUsuarioOfCafeteriaListCafeteria);
                }
            }
            for (Generarfactura generarfacturaListGenerarfactura : usuario.getGenerarfacturaList()) {
                Usuario oldUsuarioUsuarioOfGenerarfacturaListGenerarfactura = generarfacturaListGenerarfactura.getUsuarioUsuario();
                generarfacturaListGenerarfactura.setUsuarioUsuario(usuario);
                generarfacturaListGenerarfactura = em.merge(generarfacturaListGenerarfactura);
                if (oldUsuarioUsuarioOfGenerarfacturaListGenerarfactura != null) {
                    oldUsuarioUsuarioOfGenerarfacturaListGenerarfactura.getGenerarfacturaList().remove(generarfacturaListGenerarfactura);
                    oldUsuarioUsuarioOfGenerarfacturaListGenerarfactura = em.merge(oldUsuarioUsuarioOfGenerarfacturaListGenerarfactura);
                }
            }
            for (Reserva reservaListReserva : usuario.getReservaList()) {
                Usuario oldUsuariousuarioOfReservaListReserva = reservaListReserva.getUsuariousuario();
                reservaListReserva.setUsuariousuario(usuario);
                reservaListReserva = em.merge(reservaListReserva);
                if (oldUsuariousuarioOfReservaListReserva != null) {
                    oldUsuariousuarioOfReservaListReserva.getReservaList().remove(reservaListReserva);
                    oldUsuariousuarioOfReservaListReserva = em.merge(oldUsuariousuarioOfReservaListReserva);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findUsuario(usuario.getUsuario()) != null) {
                throw new PreexistingEntityException("Usuario " + usuario + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Usuario usuario) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Usuario persistentUsuario = em.find(Usuario.class, usuario.getUsuario());
            List<Asignarhab> asignarhabListOld = persistentUsuario.getAsignarhabList();
            List<Asignarhab> asignarhabListNew = usuario.getAsignarhabList();
            List<Cafeteria> cafeteriaListOld = persistentUsuario.getCafeteriaList();
            List<Cafeteria> cafeteriaListNew = usuario.getCafeteriaList();
            List<Generarfactura> generarfacturaListOld = persistentUsuario.getGenerarfacturaList();
            List<Generarfactura> generarfacturaListNew = usuario.getGenerarfacturaList();
            List<Reserva> reservaListOld = persistentUsuario.getReservaList();
            List<Reserva> reservaListNew = usuario.getReservaList();
            List<String> illegalOrphanMessages = null;
            for (Asignarhab asignarhabListOldAsignarhab : asignarhabListOld) {
                if (!asignarhabListNew.contains(asignarhabListOldAsignarhab)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Asignarhab " + asignarhabListOldAsignarhab + " since its usuarioUsuario field is not nullable.");
                }
            }
            for (Cafeteria cafeteriaListOldCafeteria : cafeteriaListOld) {
                if (!cafeteriaListNew.contains(cafeteriaListOldCafeteria)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Cafeteria " + cafeteriaListOldCafeteria + " since its usuarioUsuario field is not nullable.");
                }
            }
            for (Generarfactura generarfacturaListOldGenerarfactura : generarfacturaListOld) {
                if (!generarfacturaListNew.contains(generarfacturaListOldGenerarfactura)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Generarfactura " + generarfacturaListOldGenerarfactura + " since its usuarioUsuario field is not nullable.");
                }
            }
            for (Reserva reservaListOldReserva : reservaListOld) {
                if (!reservaListNew.contains(reservaListOldReserva)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Reserva " + reservaListOldReserva + " since its usuariousuario field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Asignarhab> attachedAsignarhabListNew = new ArrayList<Asignarhab>();
            for (Asignarhab asignarhabListNewAsignarhabToAttach : asignarhabListNew) {
                asignarhabListNewAsignarhabToAttach = em.getReference(asignarhabListNewAsignarhabToAttach.getClass(), asignarhabListNewAsignarhabToAttach.getAsignarhab());
                attachedAsignarhabListNew.add(asignarhabListNewAsignarhabToAttach);
            }
            asignarhabListNew = attachedAsignarhabListNew;
            usuario.setAsignarhabList(asignarhabListNew);
            List<Cafeteria> attachedCafeteriaListNew = new ArrayList<Cafeteria>();
            for (Cafeteria cafeteriaListNewCafeteriaToAttach : cafeteriaListNew) {
                cafeteriaListNewCafeteriaToAttach = em.getReference(cafeteriaListNewCafeteriaToAttach.getClass(), cafeteriaListNewCafeteriaToAttach.getIdCompra());
                attachedCafeteriaListNew.add(cafeteriaListNewCafeteriaToAttach);
            }
            cafeteriaListNew = attachedCafeteriaListNew;
            usuario.setCafeteriaList(cafeteriaListNew);
            List<Generarfactura> attachedGenerarfacturaListNew = new ArrayList<Generarfactura>();
            for (Generarfactura generarfacturaListNewGenerarfacturaToAttach : generarfacturaListNew) {
                generarfacturaListNewGenerarfacturaToAttach = em.getReference(generarfacturaListNewGenerarfacturaToAttach.getClass(), generarfacturaListNewGenerarfacturaToAttach.getIdFactura());
                attachedGenerarfacturaListNew.add(generarfacturaListNewGenerarfacturaToAttach);
            }
            generarfacturaListNew = attachedGenerarfacturaListNew;
            usuario.setGenerarfacturaList(generarfacturaListNew);
            List<Reserva> attachedReservaListNew = new ArrayList<Reserva>();
            for (Reserva reservaListNewReservaToAttach : reservaListNew) {
                reservaListNewReservaToAttach = em.getReference(reservaListNewReservaToAttach.getClass(), reservaListNewReservaToAttach.getIdReserva());
                attachedReservaListNew.add(reservaListNewReservaToAttach);
            }
            reservaListNew = attachedReservaListNew;
            usuario.setReservaList(reservaListNew);
            usuario = em.merge(usuario);
            for (Asignarhab asignarhabListNewAsignarhab : asignarhabListNew) {
                if (!asignarhabListOld.contains(asignarhabListNewAsignarhab)) {
                    Usuario oldUsuarioUsuarioOfAsignarhabListNewAsignarhab = asignarhabListNewAsignarhab.getUsuarioUsuario();
                    asignarhabListNewAsignarhab.setUsuarioUsuario(usuario);
                    asignarhabListNewAsignarhab = em.merge(asignarhabListNewAsignarhab);
                    if (oldUsuarioUsuarioOfAsignarhabListNewAsignarhab != null && !oldUsuarioUsuarioOfAsignarhabListNewAsignarhab.equals(usuario)) {
                        oldUsuarioUsuarioOfAsignarhabListNewAsignarhab.getAsignarhabList().remove(asignarhabListNewAsignarhab);
                        oldUsuarioUsuarioOfAsignarhabListNewAsignarhab = em.merge(oldUsuarioUsuarioOfAsignarhabListNewAsignarhab);
                    }
                }
            }
            for (Cafeteria cafeteriaListNewCafeteria : cafeteriaListNew) {
                if (!cafeteriaListOld.contains(cafeteriaListNewCafeteria)) {
                    Usuario oldUsuarioUsuarioOfCafeteriaListNewCafeteria = cafeteriaListNewCafeteria.getUsuarioUsuario();
                    cafeteriaListNewCafeteria.setUsuarioUsuario(usuario);
                    cafeteriaListNewCafeteria = em.merge(cafeteriaListNewCafeteria);
                    if (oldUsuarioUsuarioOfCafeteriaListNewCafeteria != null && !oldUsuarioUsuarioOfCafeteriaListNewCafeteria.equals(usuario)) {
                        oldUsuarioUsuarioOfCafeteriaListNewCafeteria.getCafeteriaList().remove(cafeteriaListNewCafeteria);
                        oldUsuarioUsuarioOfCafeteriaListNewCafeteria = em.merge(oldUsuarioUsuarioOfCafeteriaListNewCafeteria);
                    }
                }
            }
            for (Generarfactura generarfacturaListNewGenerarfactura : generarfacturaListNew) {
                if (!generarfacturaListOld.contains(generarfacturaListNewGenerarfactura)) {
                    Usuario oldUsuarioUsuarioOfGenerarfacturaListNewGenerarfactura = generarfacturaListNewGenerarfactura.getUsuarioUsuario();
                    generarfacturaListNewGenerarfactura.setUsuarioUsuario(usuario);
                    generarfacturaListNewGenerarfactura = em.merge(generarfacturaListNewGenerarfactura);
                    if (oldUsuarioUsuarioOfGenerarfacturaListNewGenerarfactura != null && !oldUsuarioUsuarioOfGenerarfacturaListNewGenerarfactura.equals(usuario)) {
                        oldUsuarioUsuarioOfGenerarfacturaListNewGenerarfactura.getGenerarfacturaList().remove(generarfacturaListNewGenerarfactura);
                        oldUsuarioUsuarioOfGenerarfacturaListNewGenerarfactura = em.merge(oldUsuarioUsuarioOfGenerarfacturaListNewGenerarfactura);
                    }
                }
            }
            for (Reserva reservaListNewReserva : reservaListNew) {
                if (!reservaListOld.contains(reservaListNewReserva)) {
                    Usuario oldUsuariousuarioOfReservaListNewReserva = reservaListNewReserva.getUsuariousuario();
                    reservaListNewReserva.setUsuariousuario(usuario);
                    reservaListNewReserva = em.merge(reservaListNewReserva);
                    if (oldUsuariousuarioOfReservaListNewReserva != null && !oldUsuariousuarioOfReservaListNewReserva.equals(usuario)) {
                        oldUsuariousuarioOfReservaListNewReserva.getReservaList().remove(reservaListNewReserva);
                        oldUsuariousuarioOfReservaListNewReserva = em.merge(oldUsuariousuarioOfReservaListNewReserva);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = usuario.getUsuario();
                if (findUsuario(id) == null) {
                    throw new NonexistentEntityException("The usuario with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Usuario usuario;
            try {
                usuario = em.getReference(Usuario.class, id);
                usuario.getUsuario();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The usuario with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Asignarhab> asignarhabListOrphanCheck = usuario.getAsignarhabList();
            for (Asignarhab asignarhabListOrphanCheckAsignarhab : asignarhabListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the Asignarhab " + asignarhabListOrphanCheckAsignarhab + " in its asignarhabList field has a non-nullable usuarioUsuario field.");
            }
            List<Cafeteria> cafeteriaListOrphanCheck = usuario.getCafeteriaList();
            for (Cafeteria cafeteriaListOrphanCheckCafeteria : cafeteriaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the Cafeteria " + cafeteriaListOrphanCheckCafeteria + " in its cafeteriaList field has a non-nullable usuarioUsuario field.");
            }
            List<Generarfactura> generarfacturaListOrphanCheck = usuario.getGenerarfacturaList();
            for (Generarfactura generarfacturaListOrphanCheckGenerarfactura : generarfacturaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the Generarfactura " + generarfacturaListOrphanCheckGenerarfactura + " in its generarfacturaList field has a non-nullable usuarioUsuario field.");
            }
            List<Reserva> reservaListOrphanCheck = usuario.getReservaList();
            for (Reserva reservaListOrphanCheckReserva : reservaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the Reserva " + reservaListOrphanCheckReserva + " in its reservaList field has a non-nullable usuariousuario field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(usuario);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Usuario> findUsuarioEntities() {
        return findUsuarioEntities(true, -1, -1);
    }

    public List<Usuario> findUsuarioEntities(int maxResults, int firstResult) {
        return findUsuarioEntities(false, maxResults, firstResult);
    }

    private List<Usuario> findUsuarioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Usuario.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Usuario findUsuario(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Usuario.class, id);
        } finally {
            em.close();
        }
    }

    public int getUsuarioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Usuario> rt = cq.from(Usuario.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
