/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Dao.exceptions.NonexistentEntityException;
import Dao.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Dto.Cliente;
import Dto.Guardarobjeto;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author Alexander
 */
public class GuardarobjetoJpaController implements Serializable {

    public GuardarobjetoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Guardarobjeto guardarobjeto) throws PreexistingEntityException, Exception {
        if (guardarobjeto.getClienteList() == null) {
            guardarobjeto.setClienteList(new ArrayList<Cliente>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Cliente> attachedClienteList = new ArrayList<Cliente>();
            for (Cliente clienteListClienteToAttach : guardarobjeto.getClienteList()) {
                clienteListClienteToAttach = em.getReference(clienteListClienteToAttach.getClass(), clienteListClienteToAttach.getCliente());
                attachedClienteList.add(clienteListClienteToAttach);
            }
            guardarobjeto.setClienteList(attachedClienteList);
            em.persist(guardarobjeto);
            for (Cliente clienteListCliente : guardarobjeto.getClienteList()) {
                Guardarobjeto oldObjetoOfClienteListCliente = clienteListCliente.getObjeto();
                clienteListCliente.setObjeto(guardarobjeto);
                clienteListCliente = em.merge(clienteListCliente);
                if (oldObjetoOfClienteListCliente != null) {
                    oldObjetoOfClienteListCliente.getClienteList().remove(clienteListCliente);
                    oldObjetoOfClienteListCliente = em.merge(oldObjetoOfClienteListCliente);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findGuardarobjeto(guardarobjeto.getIdObjeto()) != null) {
                throw new PreexistingEntityException("Guardarobjeto " + guardarobjeto + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Guardarobjeto guardarobjeto) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Guardarobjeto persistentGuardarobjeto = em.find(Guardarobjeto.class, guardarobjeto.getIdObjeto());
            List<Cliente> clienteListOld = persistentGuardarobjeto.getClienteList();
            List<Cliente> clienteListNew = guardarobjeto.getClienteList();
            List<Cliente> attachedClienteListNew = new ArrayList<Cliente>();
            for (Cliente clienteListNewClienteToAttach : clienteListNew) {
                clienteListNewClienteToAttach = em.getReference(clienteListNewClienteToAttach.getClass(), clienteListNewClienteToAttach.getCliente());
                attachedClienteListNew.add(clienteListNewClienteToAttach);
            }
            clienteListNew = attachedClienteListNew;
            guardarobjeto.setClienteList(clienteListNew);
            guardarobjeto = em.merge(guardarobjeto);
            for (Cliente clienteListOldCliente : clienteListOld) {
                if (!clienteListNew.contains(clienteListOldCliente)) {
                    clienteListOldCliente.setObjeto(null);
                    clienteListOldCliente = em.merge(clienteListOldCliente);
                }
            }
            for (Cliente clienteListNewCliente : clienteListNew) {
                if (!clienteListOld.contains(clienteListNewCliente)) {
                    Guardarobjeto oldObjetoOfClienteListNewCliente = clienteListNewCliente.getObjeto();
                    clienteListNewCliente.setObjeto(guardarobjeto);
                    clienteListNewCliente = em.merge(clienteListNewCliente);
                    if (oldObjetoOfClienteListNewCliente != null && !oldObjetoOfClienteListNewCliente.equals(guardarobjeto)) {
                        oldObjetoOfClienteListNewCliente.getClienteList().remove(clienteListNewCliente);
                        oldObjetoOfClienteListNewCliente = em.merge(oldObjetoOfClienteListNewCliente);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = guardarobjeto.getIdObjeto();
                if (findGuardarobjeto(id) == null) {
                    throw new NonexistentEntityException("The guardarobjeto with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Guardarobjeto guardarobjeto;
            try {
                guardarobjeto = em.getReference(Guardarobjeto.class, id);
                guardarobjeto.getIdObjeto();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The guardarobjeto with id " + id + " no longer exists.", enfe);
            }
            List<Cliente> clienteList = guardarobjeto.getClienteList();
            for (Cliente clienteListCliente : clienteList) {
                clienteListCliente.setObjeto(null);
                clienteListCliente = em.merge(clienteListCliente);
            }
            em.remove(guardarobjeto);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Guardarobjeto> findGuardarobjetoEntities() {
        return findGuardarobjetoEntities(true, -1, -1);
    }

    public List<Guardarobjeto> findGuardarobjetoEntities(int maxResults, int firstResult) {
        return findGuardarobjetoEntities(false, maxResults, firstResult);
    }

    private List<Guardarobjeto> findGuardarobjetoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Guardarobjeto.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Guardarobjeto findGuardarobjeto(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Guardarobjeto.class, id);
        } finally {
            em.close();
        }
    }

    public int getGuardarobjetoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Guardarobjeto> rt = cq.from(Guardarobjeto.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
