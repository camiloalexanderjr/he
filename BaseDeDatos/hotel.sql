-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-04-2020 a las 17:38:37
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `hotel`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignarhab`
--

CREATE TABLE `asignarhab` (
  `idAsignacion` varchar(30) NOT NULL,
  `Habitacion` varchar(30) NOT NULL,
  `Cliente` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cafeteria`
--

CREATE TABLE `cafeteria` (
  `idCompra` varchar(30) NOT NULL,
  `Cantidad` int(30) NOT NULL,
  `Producto` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja`
--

CREATE TABLE `caja` (
  `idEstado` varchar(30) NOT NULL,
  `usario_usuario` varchar(30) NOT NULL,
  `HoraInicio` varchar(30) NOT NULL,
  `HoraFin` varchar(30) NOT NULL,
  `Factura_Factura` varchar(30) NOT NULL,
  `Cafeteria_Cafeteria` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `Cliente` varchar(30) NOT NULL,
  `Nombre` varchar(30) NOT NULL,
  `Apellido` varchar(30) NOT NULL,
  `Telefono` int(30) NOT NULL,
  `Objeto` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `generarfactura`
--

CREATE TABLE `generarfactura` (
  `idFactura` varchar(30) NOT NULL,
  `AsignarHab` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `guardarobjeto`
--

CREATE TABLE `guardarobjeto` (
  `idObjeto` varchar(30) NOT NULL,
  `Nombre` varchar(50) NOT NULL,
  `Cantidad` int(30) NOT NULL,
  `Descripcion` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habitacion`
--

CREATE TABLE `habitacion` (
  `idHabitacion` varchar(30) NOT NULL,
  `Capacidad` int(30) NOT NULL,
  `Descripcion` varchar(30) NOT NULL,
  `usuario_usuario` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `limpieza`
--

CREATE TABLE `limpieza` (
  `idLimpieza` varchar(30) NOT NULL,
  `HoraInicio` varchar(30) NOT NULL,
  `HoraFin` varchar(30) NOT NULL,
  `NombreEmpleado` varchar(50) NOT NULL,
  `Observacion` varchar(100) NOT NULL,
  `Habitacion_Habitacion` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idUsuario` varchar(30) NOT NULL,
  `Contraseña` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `asignarhab`
--
ALTER TABLE `asignarhab`
  ADD PRIMARY KEY (`idAsignacion`),
  ADD KEY `Habitacion` (`Habitacion`,`Cliente`),
  ADD KEY `Cliente` (`Cliente`);

--
-- Indices de la tabla `cafeteria`
--
ALTER TABLE `cafeteria`
  ADD PRIMARY KEY (`idCompra`);

--
-- Indices de la tabla `caja`
--
ALTER TABLE `caja`
  ADD PRIMARY KEY (`idEstado`),
  ADD KEY `Factura_Factura` (`Factura_Factura`,`Cafeteria_Cafeteria`),
  ADD KEY `Cafeteria_Cafeteria` (`Cafeteria_Cafeteria`),
  ADD KEY `usario_usuario` (`usario_usuario`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`Cliente`),
  ADD KEY `Objeto` (`Objeto`);

--
-- Indices de la tabla `generarfactura`
--
ALTER TABLE `generarfactura`
  ADD PRIMARY KEY (`idFactura`),
  ADD KEY `AsignarHab` (`AsignarHab`);

--
-- Indices de la tabla `guardarobjeto`
--
ALTER TABLE `guardarobjeto`
  ADD PRIMARY KEY (`idObjeto`);

--
-- Indices de la tabla `habitacion`
--
ALTER TABLE `habitacion`
  ADD PRIMARY KEY (`idHabitacion`),
  ADD KEY `usuario_usuario` (`usuario_usuario`);

--
-- Indices de la tabla `limpieza`
--
ALTER TABLE `limpieza`
  ADD PRIMARY KEY (`idLimpieza`),
  ADD KEY `Habitacion_Habitacion` (`Habitacion_Habitacion`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idUsuario`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `asignarhab`
--
ALTER TABLE `asignarhab`
  ADD CONSTRAINT `asignarhab_ibfk_1` FOREIGN KEY (`Habitacion`) REFERENCES `habitacion` (`idHabitacion`),
  ADD CONSTRAINT `asignarhab_ibfk_2` FOREIGN KEY (`Cliente`) REFERENCES `cliente` (`Cliente`);

--
-- Filtros para la tabla `caja`
--
ALTER TABLE `caja`
  ADD CONSTRAINT `caja_ibfk_1` FOREIGN KEY (`Factura_Factura`) REFERENCES `generarfactura` (`idFactura`),
  ADD CONSTRAINT `caja_ibfk_2` FOREIGN KEY (`Cafeteria_Cafeteria`) REFERENCES `cafeteria` (`idCompra`),
  ADD CONSTRAINT `caja_ibfk_3` FOREIGN KEY (`usario_usuario`) REFERENCES `usuario` (`idUsuario`);

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `cliente_ibfk_1` FOREIGN KEY (`Objeto`) REFERENCES `guardarobjeto` (`idObjeto`);

--
-- Filtros para la tabla `generarfactura`
--
ALTER TABLE `generarfactura`
  ADD CONSTRAINT `generarfactura_ibfk_1` FOREIGN KEY (`AsignarHab`) REFERENCES `asignarhab` (`idAsignacion`);

--
-- Filtros para la tabla `habitacion`
--
ALTER TABLE `habitacion`
  ADD CONSTRAINT `habitacion_ibfk_1` FOREIGN KEY (`usuario_usuario`) REFERENCES `usuario` (`idUsuario`);

--
-- Filtros para la tabla `limpieza`
--
ALTER TABLE `limpieza`
  ADD CONSTRAINT `limpieza_ibfk_1` FOREIGN KEY (`Habitacion_Habitacion`) REFERENCES `habitacion` (`idHabitacion`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
